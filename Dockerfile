FROM node:10
COPY dist/student-class dist/student-class/
WORKDIR dist/student-class/
EXPOSE 3000
CMD npm start -D FOREGROUND --server.port=3000

