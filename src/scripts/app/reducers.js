import { fromJS } from "immutable";
import {
  SET_LOGIN_DATA,
  SET_ERRORS_LIST,
  SET_SUMMARY_LIST,
  SET_PROGRESS_LOADER,
} from "./actions";

const initialState = fromJS({
  progressLoader: true,
  loginData: {},
  errorsList: [],
  summaryList: [],
});

const login = (state = initialState, action: any) => {
  switch (action.type) {
    case SET_LOGIN_DATA:
      return state.set("loginData", fromJS({ ...action.loginData }));
    case SET_ERRORS_LIST:
      return state.set("errorsList", fromJS([...action.errors]));
    case SET_SUMMARY_LIST:
      return state.set("summaryList", fromJS([...action.summary]));
    case SET_PROGRESS_LOADER:
      return state.set("progressLoader", action.progressing);
    default:
      return state;
  }
};

export default login;
