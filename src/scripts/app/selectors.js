import { createSelector } from "reselect";
import * as selectors from "../redux/selectors";

export const getProgressLoader = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("progressLoader")
  );

export const getLoginData = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("loginData").toJS()
  );

export const getErrorsList = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("errorsList").toJS()
  );

export const getSummaryList = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("summaryList").toJS()
  );
