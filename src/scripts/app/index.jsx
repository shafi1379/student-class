import React, { Component, lazy, Suspense } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import ReactNotifications from "react-notifications-component";
import { ThemeProvider } from "@material-ui/core";
import fteTrackTheme from "./theme";
import { API_URLS } from "../utilities/constants";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import * as selectors from "./selectors";
import * as actions from "./actions";
import LoadingIndicator from "../components/generic/loadingIndicator";

const Content = lazy(() => import("../components/content"));
const Header = lazy(() => import("../components/header"));
const Footer = lazy(() => import("../components/footer"));

class App extends Component {
  componentWillMount() {
    this.props.loadLoginData({
      url: API_URLS.login,
      params: { userid: "fteadmin" },
    });
  }
  render() {
    return (
      <>
        <ReactNotifications />
        <ThemeProvider theme={fteTrackTheme}>
          <CssBaseline />
          <Suspense fallback={<LoadingIndicator />}>
            <Header />
          </Suspense>
          <div style={{ marginTop: 75 }}>
            <Suspense fallback={<LoadingIndicator />}>
              <Content />
            </Suspense>
          </div>
          <Suspense fallback={<LoadingIndicator />}>
            <Footer />
          </Suspense>
        </ThemeProvider>
      </>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    userData: selectors.getLoginData(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadLoginData: (params) => dispatch(actions.loadLoginData(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
