export const LOAD_LOGIN_DATA = "LOAD_LOGIN_DATA";
export const loadLoginData = (request) => ({
  type: LOAD_LOGIN_DATA,
  request,
});

export const SET_LOGIN_DATA = "SET_LOGIN_DATA";
export const setLoginData = (loginData) => ({
  type: SET_LOGIN_DATA,
  loginData,
});

export const SET_ERRORS_LIST = "SET_ERRORS_LIST";
export const setErrorsList = (errors) => ({
  type: SET_ERRORS_LIST,
  errors,
});

export const SET_SUMMARY_LIST = "SET_SUMMARY_LIST";
export const setSummaryList = (summary) => ({
  type: SET_SUMMARY_LIST,
  summary,
});

export const SET_PROGRESS_LOADER = "SET_PROGRESS_LOADER";
export const setProgressLoader = (progressing) => ({
  type: SET_PROGRESS_LOADER,
  progressing,
});
