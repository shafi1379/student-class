import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  LOAD_LOGIN_DATA,
  setLoginData,
  setErrorsList,
  setSummaryList,
  setProgressLoader,
} from "./actions";
import * as API from "../services/api";

function* userLoginRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    if (response) {
      if (response.loginData == null) {
        window.location.href = "../index.php";
      }
      yield put(setLoginData(response.loginData));
      yield put(setErrorsList(response.errors));
      yield put(setSummaryList(response.summary));
      yield put(setProgressLoader(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* userLoginListener() {
  yield takeLatest(LOAD_LOGIN_DATA, userLoginRequested);
}

export default function* root() {
  yield all([fork(userLoginListener)]);
}
