import { cancel, fork, take } from "redux-saga/effects";
import Login from "../app/sagas";
import StudentClass from "../components/content/student_class/sagas";
import ErrWarn from "../components/content/errors_warnings/sagas";
const sagas = [Login, StudentClass, ErrWarn];

export const CANCEL_SAGAS_HMR = "CANCEL_SAGAS_HMR";

const createAbortableSaga = (saga: any) => {
  if (process.env.NODE_ENV === "development") {
    return function* main() {
      const sagaTask = yield fork(saga);

      yield take(CANCEL_SAGAS_HMR);
      yield cancel(sagaTask);
    };
  }
  return saga;
};

const SagaManager = {
  cancelSagas: (store: any) => {
    store.dispatch({
      type: CANCEL_SAGAS_HMR,
    });
  },
  startSagas: (sagaMiddleware: any) => {
    sagas.map(createAbortableSaga).forEach((saga) => sagaMiddleware.run(saga));
  },
};

export default SagaManager;
