import { combineReducers } from "redux-immutable";
import { connectRouter } from "connected-react-router/immutable";
import login from "../app/reducers";
import studentClass from "../components/content/student_class/reducers";
import errWarn from "../components/content/errors_warnings/reducers";

const rootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    login,
    studentClass,
    errWarn,
  });

export default rootReducer;
