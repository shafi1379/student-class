export const appState = (state) => state.get("login");
export const studentClassState = (state) => state.get("studentClass");
export const errWarnState = (state) => state.get("errWarn");
