//export const BASE_URL = "https://www.ftetrack.net/api/sc/";
export const BASE_URL = "../api/sc/";

export const API_URLS = {
  login: "login",
  coursesbyschool: "coursesbyschool",
  coursesbysystem: "coursesbysystem",
  studentsbyschool: "studentsbyschool",
  teachersbyschool: "teachersbyschool",
  coursesbydepartment: "coursesbydepartment",
  sectionsbycourse: "sectionsbycourse",
  studentsinsection: "studentsinsection",
  sectionsbyteacher: "sectionsbyteacher",
  coursesforstudent: "coursesforstudent",
  roasterbyteacher: "rosterbyteacher",
  teacherassignments: "teacherassignments",
  masterschedules: "masterschedules",
  errorsummaryerr: "errorsummaryerr",
  errordetails: "errordetails",
};

/*
Student Class
=============
curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/login -d "{ \"userid\": \"fteadmin\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/summary -d "{ \"school\": \"9999\" }"
curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/summary -d "{ \"school\": \"0206\" }"
curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/coursesbyschool -d "{ \"school\": \"0206\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/coursesbysystem -d "{ \"school\": \"0206\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/studentsbyschool -d "{ \"school\": \"0206\" , \"missingCore\": \"0\" }"
curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/studentsbyschool -d "{ \"school\": \"0206\" , \"missingCore\": \"1\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/teachersbyschool -d "{ \"school\": \"0206\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/sectionsbycourse -d "{ \"school\": \"0206\" , \"course\": \"23.0610000\" }"
curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/coursesbydepartment -d "{ \"school\": \"0206\" , \"course\": \"23.0610000\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/studentsinsection -d "{ \"school\": \"0206\" , \"course\": \"23.0610000\" , \"section\": \"011\" }"
curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/coursesforstudent -d "{ \"school\": \"0206\" , \"studentid\": \"4567125526\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/sectionsbyteacher -d "{ \"school\": \"0206\" , \"teacherid\": \"234679508\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/rosterbyteacher -d "{ \"school\": \"0206\" , \"teacherid\": \"234679508\" }"
curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/rosterbyteacher -d "{ \"school\": \"0206\" , \"teacherid\": \"xxxx\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/teacherassignments -d "{ \"school\": \"0206\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/masterschedules -d "{ \"school\": \"0206\" }"

Errors And Warnings
===================
curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/errorsummaryerr -d "{ \"school\": \"9999\" , \"errCode\": \"E3027\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/errordetails -d "{ \"school\": \"0206\" , \"errCode\": \"E3027\" }"

curl -k -i -H "Content-Type: application/json" -X POST https://www.ftetrack.net/api/sc/errordetails -d "{ \"school\": \"0206\" , \"errCode\": \"E9071\" }"

*/
