export function sortArrObj(property) {
  return (a, b) => {
    if (a[property] > b[property]) {
      return 1;
    }
    if (a[property] < b[property]) {
      return -1;
    }
    return 0;
  };
}

export function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

export function formatDate(date, divider) {
  let formattedDate;
  const d = new Date(date),
    year = d.getFullYear();
  let month = "" + (d.getMonth() + 1),
    day = "" + d.getDate();
  if (month.length < 2) {
    month = "0" + month;
  }
  if (day.length < 2) {
    day = "0" + day;
  }
  if (divider) {
    formattedDate = [year, month, day].join(divider);
  } else {
    formattedDate = [year, month, day].join("-");
  }
  return formattedDate;
}

export function formatDateHyphen(date) {
  const year = date.toString().substring(0, 4);
  const month = date.toString().substring(4, 6);
  const day = date.toString().substring(6, 8);
  return [year, month, day].join("-");
}

export function formatDateSlash(date) {
  if (!date) {
    return false;
  }
  const year = date.toString().substring(0, 4);
  const month = date.toString().substring(4, 6);
  const day = date.toString().substring(6, 8);
  return [day, month, year].join("/");
}

export function generateExcelData(datas, cols) {
  let dataList = [],
    columns = {};
  if (datas.length) {
    if (isEmpty(cols)) {
      Object.keys(datas[0]).filter((attr) => (columns[attr] = attr));
    } else {
      columns = cols;
    }
    dataList = datas.map((res) => {
      const arr = [];
      for (let col in columns) {
        if (columns[col] === "school_date") {
          const date = formatDateSlash(res[columns[col]]);
          arr.push(date);
        } else {
          arr.push(res[columns[col]]);
        }
      }
      return arr;
    });
    dataList.unshift(Object.keys(columns));
  } else {
    dataList.unshift(Object.keys(cols));
  }
  return dataList;
}

export function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function generateFilteredData(item, value, objKeys, actualKeys) {
  return objKeys.filter((key) => {
    if (
      value.charAt(0) === "-" &&
      value.length > 3 &&
      value.indexOf(":") > -1
    ) {
      const newKey = value.split(":")[0].substring(1);
      const newVal = value.split(":")[1].trim();
      const actualKey = actualKeys.filter((val) => val.name === newKey);
      if (!actualKey.length && !item[newKey]) {
        return false;
      }
      if (!actualKey[0].selector && !item[actualKey[0].selector] && !newVal) {
        return true;
      } else {
        return item[actualKey[0].selector]
          .toString()
          .toLowerCase()
          .indexOf(newVal.toLowerCase()) > -1
          ? true
          : false;
      }
    } else {
      if (value.charAt(0) !== "-") {
        if (!item[key]) {
          return false;
        }
        return item[key].toString().toLowerCase().indexOf(value.toLowerCase()) >
          -1
          ? true
          : false;
      } else {
        return true;
      }
    }
  }).length > 0
    ? true
    : false;
}
export function returnBoolean(obj, sourceArr = [], value) {
  let includeValue = false;
  sourceArr.forEach((attr) => {
    if (obj[attr].toString().toLowerCase().indexOf(value.toLowerCase()) > -1) {
      includeValue = true;
    }
  });
  return includeValue;
}
