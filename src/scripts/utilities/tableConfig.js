import React from "react";

export const customStyles = {
  rows: {
    style: {
      minHeight: "35px",
    },
  },
};

export const summaryColumns = [
  {
    name: "School",
    selector: "schoolName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Grade Range",
    selector: "GradeRange",
  },
  {
    name: "# of Sections",
    selector: "sections",
    sortable: true,
  },
  {
    name: "# of Students",
    selector: "students",
    sortable: true,
  },
  {
    name: "# of Teachers",
    selector: "teachers",
    sortable: true,
  },
  {
    name: "Missing Core",
    selector: "missingCore",
    sortable: true,
  },
];

export const errorsColumns = [
  {
    name: "#",
    selector: "srlno",
    sortable: true,
  },
  {
    name: "Report Type",
    selector: "errorDesc",
    sortable: true,
    grow: 10,
  },
  {
    name: "# of Records",
    selector: "errCount",
    sortable: true,
  },
];

export const coursesBySchool = [
  {
    name: "Course #",
    selector: "Course",
    sortable: true,
  },
  {
    name: "Title",
    selector: "Title",
    sortable: true,
    grow: 2,
  },
  {
    name: "Department",
    selector: "Department",
    sortable: true,
    grow: 3,
  },
  {
    name: "# of Teachers",
    selector: "teachers",
    sortable: true,
  },
  {
    name: "# of Sections",
    selector: "sections",
    sortable: true,
  },
  {
    name: "Max",
    selector: "maxClassSize",
    sortable: true,
  },
  {
    name: "Min",
    selector: "minClassSize",
    sortable: true,
  },
  {
    name: "Avg",
    selector: "avgClassSize",
    sortable: true,
  },
];

export const coursesBySystem = [
  {
    name: "Course #",
    selector: "Course",
    sortable: true,
  },
  {
    name: "Title",
    selector: "Title",
    sortable: true,
    grow: 2,
  },
  {
    name: "Department",
    selector: "Department",
    sortable: true,
    grow: 3,
  },
  {
    name: "# of Schools",
    selector: "schools",
    sortable: true,
  },
  {
    name: "# of Sections",
    selector: "sections",
    sortable: true,
  },
  {
    name: "Max",
    selector: "maxClassSize",
    sortable: true,
  },
  {
    name: "Min",
    selector: "minClassSize",
    sortable: true,
  },
  {
    name: "Avg",
    selector: "avgClassSize",
    sortable: true,
  },
];

export const studentsBySchool = [
  {
    name: "Student ID",
    selector: "studentID",
    cell: (row) => (
      <div>******{row.studentID.substr(row.studentID.length - 4)}</div>
    ),
    sortable: true,
  },
  {
    name: "Student Name",
    selector: "studentName",
    grow: 2,
    sortable: true,
  },
  {
    name: "Grade Level",
    selector: "Grade",
    sortable: true,
  },
  {
    name: "FTE Segments",
    selector: "PROGRAM_CODES",
    sortable: true,
  },
  {
    name: "# of Classes",
    selector: "sections",
    sortable: true,
  },
  {
    name: "Reading(k-5)",
    selector: "rd",
    sortable: true,
  },
  {
    name: "Lang/Arts",
    selector: "la",
    sortable: true,
  },
  {
    name: "Math",
    selector: "ma",
    sortable: true,
  },
  {
    name: "Science",
    selector: "sc",
    sortable: true,
  },
  {
    name: "Soc Studies",
    selector: "ss",
    sortable: true,
  },
];

export const teachersBySchool = [
  {
    name: "Teacher Name",
    selector: "teacherName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Teacher ID",
    selector: "TeacherID",
    cell: (row) => (
      <div>******{row.TeacherID.substr(row.TeacherID.length - 4)}</div>
    ),
    sortable: true,
  },
  {
    name: "# of Sections",
    selector: "sections",
    sortable: true,
  },
  {
    name: "# of Courses",
    selector: "courses",
    sortable: true,
  },
  {
    name: "# of Students",
    selector: "students",
    sortable: true,
  },
];

export const coursesByDepartment = [
  {
    name: "Course #",
    selector: "Course",
    sortable: true,
  },
  {
    name: "Course Title",
    selector: "Title",
    sortable: true,
    grow: 2,
  },
  {
    name: "Teacher ID",
    selector: "TeacherID",
    cell: (row) => (
      <div>******{row.TeacherID.substr(row.TeacherID.length - 4)}</div>
    ),
    sortable: true,
  },
  {
    name: "Teacher Name",
    selector: "teacherName",
    sortable: true,
    grow: 2,
  },
  {
    name: "# of Sections",
    selector: "sections",
    sortable: true,
  },
  {
    name: "# of Students",
    selector: "students",
    sortable: true,
  },
];

export const sectionsByCourse = [
  {
    name: "Class Period",
    selector: "Period",
    sortable: true,
  },
  {
    name: "Teacher Name",
    selector: "teacherName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Term Code",
    selector: "TermCode",
    sortable: true,
  },
  {
    name: "Sections #",
    selector: "Section",
    sortable: true,
  },
  {
    name: "# of Students",
    selector: "classSize",
    sortable: true,
  },
];

export const systemsByCourse = [
  {
    name: "School",
    selector: "schoolName",
    sortable: true,
  },
  {
    name: "Class Period",
    selector: "Period",
    sortable: true,
  },
  {
    name: "Teacher Name",
    selector: "teacherName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Term Code",
    selector: "TermCode",
    sortable: true,
  },
  {
    name: "Sections #",
    selector: "Section",
    sortable: true,
  },
  {
    name: "# of Students",
    selector: "classSize",
    sortable: true,
  },
];

export const studentsInSection = [
  {
    name: "Class Period",
    selector: "Period",
    sortable: true,
  },
  {
    name: "Teacher Name",
    selector: "teacherName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Student ID",
    selector: "studentID",
    cell: (row) => (
      <div>******{row.studentID.substr(row.studentID.length - 4)}</div>
    ),
    sortable: true,
  },
  {
    name: "Student Name",
    selector: "studentName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Grade",
    selector: "Grade",
    sortable: true,
  },
  {
    name: "FTE Segments",
    selector: "PROGRAM_CODES",
    sortable: true,
  },
];

export const sectionsByStudent = [
  {
    name: "Class Period",
    selector: "Period",
    sortable: true,
  },
  {
    name: "Teacher Name",
    selector: "teacherName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Student ID",
    selector: "studentID",
    cell: (row) => (
      <div>******{row.studentID.substr(row.studentID.length - 4)}</div>
    ),
    sortable: true,
  },
  {
    name: "Student Name",
    selector: "studentName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Grade",
    selector: "Grade",
    sortable: true,
  },
  {
    name: "FTE Segments",
    selector: "PROGRAM_CODES",
    sortable: true,
  },
];

export const sectionsByTeacher = [
  {
    name: "Period (Total Students) / Section #",
    selector: "Section",
    sortable: true,
    width: "260px",
  },
  {
    name: "Course",
    selector: "Course",
    sortable: true,
    width: "260px",
  },
  {
    name: "Course Title",
    selector: "Title",
    sortable: true,
    width: "260px",
  },
  {
    name: "Term Code",
    selector: "TermCode",
    sortable: true,
    width: "260px",
  },
  {
    name: "# of Students",
    selector: "students",
    sortable: true,
    width: "260px",
  },
];

export const coursesForStudentColumns = [
  {
    name: "Period",
    selector: "Period",
    sortable: true,
    width: "60px",
  },
  {
    name: "Course #",
    selector: "Course",
    sortable: true,
    width: "105px",
  },
  {
    name: "Course Title",
    selector: "Title",
    sortable: true,
    width: "206px",
  },
  {
    name: "Section",
    selector: "Section",
    sortable: true,
    width: "60px",
  },
  {
    name: "Term",
    selector: "TermCode",
    sortable: true,
    width: "60px",
  },
  {
    name: "Teacher Name",
    selector: "teacherName",
    sortable: true,
    width: "190px",
  },
  {
    name: "Start Dt",
    selector: "startDate",
    sortable: true,
    width: "100px",
  },
  {
    name: "End Dt",
    selector: "endDate",
    sortable: true,
    width: "100px",
  },
];

export const rosterByTeacherColumns = [
  {
    name: "Term",
    selector: "TermCode",
    sortable: true,
    width: "60px",
  },
  {
    name: "Section",
    selector: "Section",
    sortable: true,
    width: "130px",
  },
  {
    name: "Course Title",
    selector: "Title",
    sortable: true,
    width: "215px",
  },
  {
    name: "Period",
    selector: "Period",
    sortable: true,
    width: "60px",
  },
  {
    name: "Student Name",
    selector: "studentName",
    sortable: true,
    width: "214px",
  },
  {
    name: "Start Dt",
    selector: "startDate",
    sortable: true,
    width: "100px",
  },
  {
    name: "End Dt",
    selector: "endDate",
    sortable: true,
    width: "100px",
  },
];

export const errWarnSchool = [
  {
    name: "School #",
    selector: "schoolCode",
    sortable: true,
  },
  {
    name: "School Name",
    selector: "schoolName",
    sortable: true,
    grow: 8,
  },
  {
    name: "# of Classes",
    selector: "errCount",
    sortable: true,
  },
];

export const errWarnStudent = [
  {
    name: "Student ID",
    selector: "StudentID",
    cell: (row) => (
      <div>******{row.StudentID.substr(row.StudentID.length - 4)}</div>
    ),
    sortable: true,
  },
  {
    name: "Student Name",
    selector: "studentName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Course #",
    selector: "courseNumber",
    sortable: true,
  },
  {
    name: "Field Name",
    selector: "FIELD_NAME",
    sortable: true,
    grow: 2,
  },
  {
    name: "Field Content",
    selector: "FIELD_CONTENT",
    sortable: true,
    grow: 2,
  },
];

export const teachersAssignments = [
  {
    name: "Teacher ID",
    selector: "TeacherID",
    sortable: true,
  },
  {
    name: "Teacher Name",
    selector: "teacherName",
    sortable: true,
    grow: 2,
  },
  {
    name: "Course #",
    selector: "CourseCategory",
    sortable: true,
  },
  {
    name: "Assignment",
    selector: "Department",
    sortable: true,
    grow: 3,
  },
];
