import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import MaterialIcon from "../generic//materialIcon";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as appSelectors from "../../app/selectors";

class AppHeader extends Component {
  logout = () => {
    if (window.location.host === "www.ftetrack.net") {
      window.location.href = "https://www.ftetrack.net/index.php";
    } else {
      window.location.href = "../index.php";
    }
  };

  handleHeader = () => {
    window.location.href = "../index.php";
  };

  render() {
    const iconPath = process.env.PUBLIC_URL + "/static/media";
    const { loginData } = this.props;
    return (
      <div className="app-header">
        <AppBar
          color="default"
          position="fixed"
          style={{
            boxShadow:
              "0px 0px 1px -1px rgba(0,0,0,0.2), 0px 1px 0px 0px rgba(0,0,0,0.14), 0px 1px 0px 0px rgba(0,0,0,0.12)",
          }}
        >
          <Toolbar variant="dense">
            <Typography
              className="app-title capitalize"
              variant="button"
              color="inherit"
            >
              <img
                onClick={this.handleHeader}
                style={{ width: 200, height: 65, cursor: "pointer" }}
                src={`${iconPath}/sa.png`}
                alt="progress-grade"
              />
            </Typography>
            <Typography
              className="last-updated mr-rt15"
              variant="caption"
              gutterBottom
              style={{ color: "#4050b5", margin: "auto" }}
            >
              Data Updated:
              <strong className="pd-lt5 red-color">
                {loginData.last_updated_sc}
              </strong>
            </Typography>
            <Tooltip title="Logout" aria-label="logout-tooltip">
              <IconButton
                onClick={this.logout}
                className="logout-btn"
                color="primary"
                aria-label="logout"
              >
                <MaterialIcon icon="power_settings_new" />
              </IconButton>
            </Tooltip>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    loginData: appSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AppHeader);
