import jsPDF from "jspdf";
import "jspdf-autotable";
import { all, call, fork, put, select, takeLatest } from "redux-saga/effects";
import {
  LOAD_COURSES_BY_SCHOOL,
  LOAD_COURSES_BY_SYSTEM,
  LOAD_STUDENTS_BY_SCHOOL,
  LOAD_TEACHERS_BY_SCHOOL,
  LOAD_COURSES_BY_DEPARTMENT,
  LOAD_SECTIONS_BY_COURSE,
  LOAD_STUDENTS_IN_SECTION,
  LOAD_SECTIONS_BY_TEACHER,
  LOAD_COURSES_FOR_STUDENT,
  LOAD_ROASTER_BY_TEACHER,
  LOAD_TEACHERS_ASSIGNMENTS,
  LOAD_MASTER_SCHEDULES,
  setCoursesBySchool,
  setFilteredCoursesBySchool,
  setStudentsBySchool,
  setFilteredStudentsBySchool,
  setTeachersBySchool,
  setFilteredTeachersBySchool,
  setCoursesByDepartment,
  setSectionsByCourse,
  setStudentsInSection,
  setFilteredStudentsInSection,
  setSectionsByTeacher,
  setFilteredStudentBySchool,
  setCoursesForStudent,
  setRoasterByTeacher,
  setTeachersAssignments,
  setMasterSchedules,
} from "./actions";
import { setProgressLoader } from "../../../app/actions";
import * as API from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";

function* loadCoursesBySchoolRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setCoursesBySchool(response));
    yield put(setFilteredCoursesBySchool(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* coursesBySchoolListener() {
  yield takeLatest(LOAD_COURSES_BY_SCHOOL, loadCoursesBySchoolRequested);
}

function* loadCoursesBySystemRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setCoursesBySchool(response));
    yield put(setFilteredCoursesBySchool(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* coursesBySystemListener() {
  yield takeLatest(LOAD_COURSES_BY_SYSTEM, loadCoursesBySystemRequested);
}

function* loadStudentsBySchoolRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setStudentsBySchool(response));
    yield put(setFilteredStudentsBySchool(response));
    yield put(setFilteredStudentBySchool(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* studentsBySchoolListener() {
  yield takeLatest(LOAD_STUDENTS_BY_SCHOOL, loadStudentsBySchoolRequested);
}

function* loadTeachersBySchoolRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setTeachersBySchool(response));
    yield put(setFilteredTeachersBySchool(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* teachersBySchoolListener() {
  yield takeLatest(LOAD_TEACHERS_BY_SCHOOL, loadTeachersBySchoolRequested);
}

function* loadCoursesByDepartmentRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setCoursesByDepartment(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* coursesByDepartmentListener() {
  yield takeLatest(
    LOAD_COURSES_BY_DEPARTMENT,
    loadCoursesByDepartmentRequested
  );
}

function* loadSectionsByCourseRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setSectionsByCourse(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* sectionsByCourseListener() {
  yield takeLatest(LOAD_SECTIONS_BY_COURSE, loadSectionsByCourseRequested);
}

function* loadStudentsInSectionRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setStudentsInSection(response));
    yield put(setFilteredStudentsInSection(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* studentsInSectionListener() {
  yield takeLatest(LOAD_STUDENTS_IN_SECTION, loadStudentsInSectionRequested);
}

function* loadSectionsByTeacherRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setSectionsByTeacher(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* sectionsByTeacherListener() {
  yield takeLatest(LOAD_SECTIONS_BY_TEACHER, loadSectionsByTeacherRequested);
}

function* loadCoursesForStudentRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(
      API.doPostRequest,
      API_URLS.coursesforstudent,
      request
    );
    yield put(setCoursesForStudent(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* coursesForStudentListener() {
  yield takeLatest(LOAD_COURSES_FOR_STUDENT, loadCoursesForStudentRequested);
}

function* loadRoasterByTeacherRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(
      API.doPostRequest,
      API_URLS.roasterbyteacher,
      request
    );
    if (request.teacherid !== "xxxx") {
      yield put(setRoasterByTeacher(response));
    } else {
      if (response) {
        const doc = new jsPDF("p", "pt");
        const schoolName = request.schoolName || response[0].schoolName;
        const loginData = (yield select((s: any) => s.get("login").get("loginData"))).toJS();
        const mainHeader = loginData.system_name;
        var pageDataMapping = {};
        response.forEach((obj) => {
          if (
            Object.keys(pageDataMapping).includes(
              `${obj.teacherName} | ${obj.Section} | ${obj.TermCode} | ${obj.AddlTeacher}`
            )
          ) {
            pageDataMapping[
              `${obj.teacherName} | ${obj.Section} | ${obj.TermCode} | ${obj.AddlTeacher}`
            ].push(obj);
          } else {
            pageDataMapping[
              `${obj.teacherName} | ${obj.Section} | ${obj.TermCode} | ${obj.AddlTeacher}`
            ] = [obj];
          }
        });
        let index = 0;
        for (let res in pageDataMapping) {
          let lastTableOffset = 105;
          if (index > 0) {
            doc.addPage();
          }
          const currentDate = new Date().toLocaleDateString();
          doc.setFontSize(9);
          doc.text(
            currentDate,
            doc.internal.pageSize.width -
              doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
              50,
            45
          );
          const xOffsetH1 =
            doc.internal.pageSize.width / 2 -
            (doc.getStringUnitWidth(mainHeader) * doc.internal.getFontSize()) /
              2;
          doc.setFontSize(12);
          doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 45);
          doc.setFontSize(10);
          const xOffsetH2 =
            doc.internal.pageSize.width / 2 -
            (doc.getStringUnitWidth(schoolName) * doc.internal.getFontSize()) /
              2;
          doc.text(schoolName, xOffsetH2, 60);
          doc.setFontSize(8);
          const xOffsetRoaster =
            doc.internal.pageSize.width / 2 -
            (doc.getStringUnitWidth(
              `Roster for Teacher: ${pageDataMapping[res][0].teacherName}`
            ) *
              doc.internal.getFontSize()) /
              2;
          doc.text(
            `Roster for Teacher: ${pageDataMapping[res][0].teacherName}`,
            xOffsetRoaster,
            75
          );
          const xOffsetSection =
            doc.internal.pageSize.width / 2 -
            (doc.getStringUnitWidth(
              `Section: ${pageDataMapping[res][0].Section} (${pageDataMapping[res][0].Title}) - ${pageDataMapping[res][0].TermCode}   `
            ) *
              doc.internal.getFontSize()) /
              2;
          doc.text(
            `Section: ${pageDataMapping[res][0].Section} `,
            xOffsetSection,
            90
          );
          const xOffset1 =
            xOffsetSection +
            doc.getStringUnitWidth(pageDataMapping[res][0].Section) *
              doc.internal.getFontSize() +
            35;

          doc.text(`(${pageDataMapping[res][0].Title})`, xOffset1, 90);
          const xOffset2 =
            xOffset1 +
            doc.getStringUnitWidth(pageDataMapping[res][0].Title) *
              doc.internal.getFontSize() +
            5;
          if (pageDataMapping[res][0].TermCode) {
            doc.text(`- ${pageDataMapping[res][0].TermCode}`, xOffset2 + 5, 90);
          }
          if (pageDataMapping[res][0].AddlTeacher) {
            doc.text(
              `(Addl Tchr: ${pageDataMapping[res][0].AddlTeacher})`,
              xOffset2 + 20,
              90
            );
          }
          doc.line(
            20,
            doc.internal.pageSize.height - 50,
            200,
            doc.internal.pageSize.height - 50
          );
          doc.text(
            `${pageDataMapping[res][0].teacherName}`,
            70,
            doc.internal.pageSize.height - 35
          );
          const columns = [
            { title: "Class Period", dataKey: "Period" },
            { title: "Student Name", dataKey: "studentName" },
            { title: "Grade", dataKey: "Grade" },
            { title: "Start Dt", dataKey: "startDate" },
            { title: "End Dt", dataKey: "endDate" },
            { title: "EIP Del", dataKey: "EIPDeliverModel" },
            { title: "Gifted Del", dataKey: "GiftedDeliverModel" },
            { title: "ESOL Del", dataKey: "ESOLDeliverModel" },
            { title: "Inclusion", dataKey: "InclusionCode" },
            { title: "SPED Del", dataKey: "SPEDDeliveryModel" },
          ];
          const autoCalculateMargin = { top: 105 };

          const rows = [...pageDataMapping[res]];

          doc.autoTable(columns, rows, {
            margin: autoCalculateMargin,
            headStyles: { fontSize: 7 },
            bodyStyles: { fontSize: 7 },
            pageBreak: "avoid",
            rowHeight: 10,
            rowPageBreak: "avoid",
            startY: lastTableOffset + 10,
            didParseCell: function (data) {},
            didDrawPage: function (data) {
              doc.line(
                doc.internal.pageSize.width / 2 - 150,
                data.cursor.y + 10,
                doc.internal.pageSize.width / 2 + 150,
                data.cursor.y + 10
              );
              doc.text(
                `Total # of Students in Section: ${pageDataMapping[res].length}`,
                doc.internal.pageSize.width / 2 - 50,
                data.cursor.y + 25
              );
            },
          });
          index++;
        }
        doc.save(`TeacherRoster.pdf`);
      }

      console.log("Download PDF");
    }
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* roasterByTeacherListener() {
  yield takeLatest(LOAD_ROASTER_BY_TEACHER, loadRoasterByTeacherRequested);
}

function* loadTeachersAssignmentsRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(
      API.doPostRequest,
      API_URLS.teacherassignments,
      request
    );
    yield put(setTeachersAssignments(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* teachersAssignmentsListener() {
  yield takeLatest(LOAD_TEACHERS_ASSIGNMENTS, loadTeachersAssignmentsRequested);
}

function* loadMasterSchedulesRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(
      API.doPostRequest,
      API_URLS.masterschedules,
      request
    );
    yield put(setMasterSchedules(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* masterSchedulesListener() {
  yield takeLatest(LOAD_MASTER_SCHEDULES, loadMasterSchedulesRequested);
}

export default function* root() {
  yield all([
    fork(coursesBySchoolListener),
    fork(coursesBySystemListener),
    fork(studentsBySchoolListener),
    fork(teachersBySchoolListener),
    fork(coursesByDepartmentListener),
    fork(sectionsByCourseListener),
    fork(studentsInSectionListener),
    fork(sectionsByTeacherListener),
    fork(coursesForStudentListener),
    fork(roasterByTeacherListener),
    fork(teachersAssignmentsListener),
    fork(masterSchedulesListener),
  ]);
}
