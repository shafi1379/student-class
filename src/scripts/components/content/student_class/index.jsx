import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import DataTable from "react-data-table-component";
import * as appSelectors from "../../../app/selectors";
import * as actions from "./actions";
import * as selectors from "./selectors";
import * as TableConfig from "../../../utilities/tableConfig";
import * as Utilities from "../../../utilities/utilities";
import MaterialIcon from "../../generic/materialIcon";
import { CSVLink } from "react-csv";
import { API_URLS } from "../../../utilities/constants";
import Sections from "./containers/sections";
import Systems from "./containers/systems";
import Students from "./containers/students";
import Teachers from "./containers/teachers";
import CourseDepartments from "./containers/sections/containers/coursedepartments";
import CourseSections from "./containers/sections/containers/coursesections";
import SystemSections from "./containers/systems/containers/systemsections";
import StudentsInSection from "./containers/sections/containers/studentsinsection";
import SectionByTeacher from "./containers/teachers/containers/sectionsbyteacher";
import { anchorLinkStyle } from "./containers/filters";

class StudentClass extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "student-class.csv",
    },
    columns: TableConfig.summaryColumns.map((dash, index) => {
      if (dash.selector === "sections") {
        dash.cell = (row) => (
          <span
            style={Number(row.sections) > 0 ? anchorLinkStyle : {}}
            onClick={this.handleAction.bind(
              this,
              row,
              Number(row.sections) > 0 ? "sections" : null
            )}
          >
            {row.sections}
          </span>
        );
      }
      if (dash.selector === "students") {
        dash.cell = (row) => (
          <span
            style={Number(row.students) > 0 ? anchorLinkStyle : {}}
            onClick={this.handleAction.bind(
              this,
              row,
              Number(row.students) > 0 ? "students" : null
            )}
          >
            {row.students}
          </span>
        );
      }
      if (dash.selector === "teachers") {
        dash.cell = (row) => (
          <span
            style={Number(row.teachers) > 0 ? anchorLinkStyle : {}}
            onClick={this.handleAction.bind(
              this,
              row,
              Number(row.teachers) > 0 ? "teachers" : null
            )}
          >
            {row.teachers}
          </span>
        );
      }
      if (dash.selector === "missingCore") {
        dash.cell = (row) => (
          <span
            style={Number(row.missingCore) > 0 ? anchorLinkStyle : {}}
            onClick={this.handleAction.bind(
              this,
              row,
              Number(row.missingCore) > 0 ? "missingCore" : null
            )}
          >
            {row.missingCore}
          </span>
        );
      }
      return dash;
    }),
  };

  handleAction = (row, cellName) => {
    if (cellName != null) {
      this.props.setSelectedSchoolRow(row);
      if (cellName === "sections") {
        if (row.schoolCode === "9999") {
          this.props.setMainHeader("Courses By System");
          const mainHeaders = { ...this.props.nestedMainHeaders };
          this.props.setNestedMainHeaders({
            ...mainHeaders,
            systems: "Courses By System",
          });
          this.props.loadCoursesBySystem({
            url: API_URLS.coursesbysystem,
            params: { school: row.schoolCode },
          });
          this.props.setCurrentTable("systems");
        } else {
          this.props.setMainHeader("Courses By School");
          const mainHeaders = { ...this.props.nestedMainHeaders };
          this.props.setNestedMainHeaders({
            ...mainHeaders,
            sections: "Courses By School",
          });
          this.props.loadCoursesBySchool({
            url: API_URLS.coursesbyschool,
            params: { school: row.schoolCode },
          });
          this.props.setCurrentTable("sections");
        }
      } else if (cellName === "teachers") {
        this.props.setMainHeader("Teachers By School Summary");
        const mainHeaders = { ...this.props.nestedMainHeaders };
        this.props.setNestedMainHeaders({
          ...mainHeaders,
          teachers: "Teachers By School Summary",
        });
        this.props.loadTeachersBySchool({
          url: API_URLS.teachersbyschool,
          params: { school: row.schoolCode },
        });
        this.props.setCurrentTable("teachers");
      } else {
        this.props.setMainHeader("Students By School");
        const mainHeaders = { ...this.props.nestedMainHeaders };
        this.props.setNestedMainHeaders({
          ...mainHeaders,
          students: "Students By School",
        });
        this.props.loadStudentsBySchool({
          url: API_URLS.studentsbyschool,
          params: {
            school: row.schoolCode,
            missingCore: cellName === "students" ? 0 : 1,
          },
        });
        if (cellName === "missingCore") {
          this.props.setCurrentTable("missingCore");
        } else {
          this.props.setCurrentTable("students");
        }
      }
    }
    this.props.setSubHeader(row.schoolName);
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const summaryList = this.props.summaryList;
    const columns = {
      School: "schoolName",
      "Grade Range": "GradeRange",
      "# of Sections": "sections",
      "# of Students": "students",
      "# of Teachers": "teachers",
      "Missing Core": "missingCore",
    };
    const data = Utilities.generateExcelData(summaryList || [], columns || {});
    data.unshift(["System Totals"]);
    data.unshift(["Section By School Summary"]);
    this.setState({
      csvExport: { data, fileName: `Schedule_Analysis_${currentTime}` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  goBack = () => {
    const currentTable = this.props.currentTable;
    const lastIndex = currentTable.lastIndexOf("-");
    const route = currentTable.substr(0, lastIndex);
    const nestedMainHeaders = this.props.nestedMainHeaders;
    if (route === "") {
      this.props.setSubHeader("System Totals");
    } else if (route === "systems-sections") {
      this.props.setSubHeader("9999 - System Totals");
    }
    const mainHeader = nestedMainHeaders[route || "default"];
    this.props.setMainHeader(mainHeader);
    this.props.setCurrentTable(route);
  };

  render() {
    const { columns, csvExport } = this.state;
    const { summaryList, mainHeader, subHeader, currentTable } = this.props;
    let dataTable = null;
    if (currentTable === "sections") {
      dataTable = <Sections />;
    } else if (currentTable === "systems") {
      dataTable = <Systems />;
    } else if (currentTable === "students" || currentTable === "missingCore") {
      dataTable = <Students />;
    } else if (currentTable === "teachers") {
      dataTable = <Teachers />;
    } else if (currentTable === "sections-departments") {
      dataTable = <CourseDepartments />;
    } else if (currentTable === "sections-sections") {
      dataTable = <CourseSections />;
    } else if (currentTable === "systems-sections") {
      dataTable = <SystemSections />;
    } else if (currentTable === "teachers-sections") {
      dataTable = <SectionByTeacher />;
    } else if (
      currentTable === "sections-sections-students" ||
      currentTable === "systems-sections-students" ||
      currentTable === "teachers-sections-students"
    ) {
      dataTable = <StudentsInSection />;
    } else {
      dataTable = (
        <DataTable
          noHeader={true}
          striped={true}
          fixedHeader={true}
          className="dataTable-override"
          fixedHeaderScrollHeight={"68vh"}
          columns={columns}
          data={summaryList || [{}]}
          customStyles={TableConfig.customStyles}
        />
      );
    }
    return (
      <div className="student-class">
        <Typography
          className="mr-tp10 yellow-color font16"
          variant="subtitle2"
          align="center"
        >
          {mainHeader}
        </Typography>
        <Typography
          variant="subtitle2"
          align="center"
          style={{ position: "relative", color: "#4050B5" }}
        >
          {currentTable && (
            <span
              style={{
                color: "#4050B5",
                textDecoration: "underline",
                cursor: "pointer",
                position: "absolute",
                left: 20,
              }}
              onClick={this.goBack}
            >
              Back
            </span>
          )}
          {subHeader}
          {!currentTable && (
            <span
              style={{
                position: "absolute",
                right: 20,
                top: -20,
              }}
            >
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  onClick={this.downloadCSV}
                  className="tools-btn"
                  color="primary"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
              <CSVLink
                ref={this.csvLink}
                data={csvExport.data}
                className="hidden"
                filename={csvExport.fileName}
                target="_self"
              />
            </span>
          )}
        </Typography>
        <Grid container>
          <Grid
            item
            xs={12}
            style={{ textAlign: "right", paddingRight: 15 }}
          ></Grid>
        </Grid>
        <Paper style={{ marginTop: 10 }}>{dataTable}</Paper>
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    summaryList: appSelectors.getSummaryList(),
    mainHeader: selectors.getMainHeader(),
    subHeader: selectors.getSubHeader(),
    coursesBySchool: selectors.getCoursesBySchool(),
    studentsBySchool: selectors.getStudentsBySchool(),
    teachersBySchool: selectors.getTeachersBySchool(),
    currentTable: selectors.getCurrentTable(),
    nestedMainHeaders: selectors.getNestedMainHeaders(),
  });
const mapDispatchToProps = (dispatch) => ({
  setMainHeader: (request) => dispatch(actions.setMainHeader(request)),
  setSubHeader: (request) => dispatch(actions.setSubHeader(request)),
  loadCoursesBySchool: (request) =>
    dispatch(actions.loadCoursesBySchool(request)),
  loadCoursesBySystem: (request) =>
    dispatch(actions.loadCoursesBySystem(request)),
  loadStudentsBySchool: (request) =>
    dispatch(actions.loadStudentsBySchool(request)),
  loadTeachersBySchool: (request) =>
    dispatch(actions.loadTeachersBySchool(request)),
  setCurrentTable: (request) => dispatch(actions.setCurrentTable(request)),
  setNestedMainHeaders: (request) =>
    dispatch(actions.setNestedMainHeaders(request)),
  setSelectedSchoolRow: (request) =>
    dispatch(actions.setSelectedSchoolRow(request)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(StudentClass));
