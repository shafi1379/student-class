export const SET_MAIN_HEADER = "SET_MAIN_HEADER";
export const setMainHeader = (mainHeader) => ({
  type: SET_MAIN_HEADER,
  mainHeader,
});

export const SET_SUB_HEADER = "SET_SUB_HEADER";
export const setSubHeader = (subHeader) => ({
  type: SET_SUB_HEADER,
  subHeader,
});

export const LOAD_COURSES_BY_SCHOOL = "LOAD_COURSES_BY_SCHOOL";
export const loadCoursesBySchool = (request) => ({
  type: LOAD_COURSES_BY_SCHOOL,
  request,
});

export const SET_COURSES_BY_SCHOOL = "SET_COURSES_BY_SCHOOL";
export const setCoursesBySchool = (courses) => ({
  type: SET_COURSES_BY_SCHOOL,
  courses,
});

export const LOAD_COURSES_BY_SYSTEM = "LOAD_COURSES_BY_SYSTEM";
export const loadCoursesBySystem = (request) => ({
  type: LOAD_COURSES_BY_SYSTEM,
  request,
});

export const LOAD_STUDENTS_BY_SCHOOL = "LOAD_STUDENTS_BY_SCHOOL";
export const loadStudentsBySchool = (request) => ({
  type: LOAD_STUDENTS_BY_SCHOOL,
  request,
});

export const SET_STUDENTS_BY_SCHOOL = "SET_STUDENTS_BY_SCHOOL";
export const setStudentsBySchool = (students) => ({
  type: SET_STUDENTS_BY_SCHOOL,
  students,
});

export const SET_FILTERED_STUDENT_BY_SCHOOL = "SET_FILTERED_STUDENT_BY_SCHOOL";
export const setFilteredStudentBySchool = (students) => ({
  type: SET_FILTERED_STUDENT_BY_SCHOOL,
  students,
});

export const LOAD_TEACHERS_BY_SCHOOL = "LOAD_TEACHERS_BY_SCHOOL";
export const loadTeachersBySchool = (request) => ({
  type: LOAD_TEACHERS_BY_SCHOOL,
  request,
});

export const SET_TEACHERS_BY_SCHOOL = "SET_TEACHERS_BY_SCHOOL";
export const setTeachersBySchool = (teachers) => ({
  type: SET_TEACHERS_BY_SCHOOL,
  teachers,
});

export const SET_CURRENT_TABLE = "SET_CURRENT_TABLE";
export const setCurrentTable = (table) => ({
  type: SET_CURRENT_TABLE,
  table,
});

export const LOAD_COURSES_BY_DEPARTMENT = "LOAD_COURSES_BY_DEPARTMENT";
export const loadCoursesByDepartment = (request) => ({
  type: LOAD_COURSES_BY_DEPARTMENT,
  request,
});

export const SET_COURSES_BY_DEPARTMENT = "SET_COURSES_BY_DEPARTMENT";
export const setCoursesByDepartment = (departments) => ({
  type: SET_COURSES_BY_DEPARTMENT,
  departments,
});

export const LOAD_SECTIONS_BY_COURSE = "LOAD_SECTIONS_BY_COURSE";
export const loadSectionsByCourse = (request) => ({
  type: LOAD_SECTIONS_BY_COURSE,
  request,
});

export const SET_SECTIONS_BY_COURSE = "SET_SECTIONS_BY_COURSE";
export const setSectionsByCourse = (sections) => ({
  type: SET_SECTIONS_BY_COURSE,
  sections,
});

export const LOAD_STUDENTS_IN_SECTION = "LOAD_STUDENTS_IN_SECTION";
export const loadStudentsInSection = (request) => ({
  type: LOAD_STUDENTS_IN_SECTION,
  request,
});

export const SET_STUDENTS_IN_SECTION = "SET_STUDENTS_IN_SECTION";
export const setStudentsInSection = (students) => ({
  type: SET_STUDENTS_IN_SECTION,
  students,
});

export const SET_FILTERED_STUDENTS_IN_SECTION =
  "SET_FILTERED_STUDENTS_IN_SECTION";
export const setFilteredStudentsInSection = (students) => ({
  type: SET_FILTERED_STUDENTS_IN_SECTION,
  students,
});

export const LOAD_SECTIONS_BY_TEACHER = "LOAD_SECTIONS_BY_TEACHER";
export const loadSectionsByTeacher = (request) => ({
  type: LOAD_SECTIONS_BY_TEACHER,
  request,
});

export const SET_SECTIONS_BY_TEACHER = "SET_SECTIONS_BY_TEACHER";
export const setSectionsByTeacher = (teachers) => ({
  type: SET_SECTIONS_BY_TEACHER,
  teachers,
});

export const SET_SELECTED_SCHOOL_ROW = "SET_SELECTED_SCHOOL_ROW";
export const setSelectedSchoolRow = (schoolRow) => ({
  type: SET_SELECTED_SCHOOL_ROW,
  schoolRow,
});

export const SET_SELECTED_COURSE_SECTION = "SET_SELECTED_COURSE_SECTION";
export const setSelectedCourseSection = (courseDetails) => ({
  type: SET_SELECTED_COURSE_SECTION,
  courseDetails,
});

export const SET_NESTED_MAIN_HEADERS = "SET_NESTED_MAIN_HEADERS";
export const setNestedMainHeaders = (headers) => ({
  type: SET_NESTED_MAIN_HEADERS,
  headers,
});

export const LOAD_COURSES_FOR_STUDENT = "LOAD_COURSES_FOR_STUDENT";
export const loadCoursesForStudent = (request) => ({
  type: LOAD_COURSES_FOR_STUDENT,
  request,
});

export const SET_COURSES_FOR_STUDENT = "SET_COURSES_FOR_STUDENT";
export const setCoursesForStudent = (courses) => ({
  type: SET_COURSES_FOR_STUDENT,
  courses,
});

export const LOAD_ROASTER_BY_TEACHER = "LOAD_ROASTER_BY_TEACHER";
export const loadRoasterByTeacher = (request) => ({
  type: LOAD_ROASTER_BY_TEACHER,
  request,
});

export const SET_ROASTER_BY_TEACHER = "SET_ROASTER_BY_TEACHER";
export const setRoasterByTeacher = (roasterByTeacher) => ({
  type: SET_ROASTER_BY_TEACHER,
  roasterByTeacher,
});

export const LOAD_TEACHERS_ASSIGNMENTS = "LOAD_TEACHERS_ASSIGNMENTS";
export const loadTeachersAssignments = (request) => ({
  type: LOAD_TEACHERS_ASSIGNMENTS,
  request,
});

export const SET_TEACHERS_ASSIGNMENTS = "SET_TEACHERS_ASSIGNMENTS";
export const setTeachersAssignments = (teachersAssignments) => ({
  type: SET_TEACHERS_ASSIGNMENTS,
  teachersAssignments,
});

export const LOAD_MASTER_SCHEDULES = "LOAD_MASTER_SCHEDULES";
export const loadMasterSchedules = (request) => ({
  type: LOAD_MASTER_SCHEDULES,
  request,
});

export const SET_MASTER_SCHEDULES = "SET_MASTER_SCHEDULES";
export const setMasterSchedules = (masterSchedules) => ({
  type: SET_MASTER_SCHEDULES,
  masterSchedules,
});

export const SET_FILTERED_COURSES_BY_SCHOOL = "SET_FILTERED_COURSES_BY_SCHOOL";
export const setFilteredCoursesBySchool = (courses) => ({
  type: SET_FILTERED_COURSES_BY_SCHOOL,
  courses,
});

export const SET_FILTERED_STUDENTS_BY_SCHOOL =
  "SET_FILTERED_STUDENTS_BY_SCHOOL";
export const setFilteredStudentsBySchool = (students) => ({
  type: SET_FILTERED_STUDENTS_BY_SCHOOL,
  students,
});

export const SET_FILTERED_TEACHERS_BY_SCHOOL =
  "SET_FILTERED_TEACHERS_BY_SCHOOL";
export const setFilteredTeachersBySchool = (teachers) => ({
  type: SET_FILTERED_TEACHERS_BY_SCHOOL,
  teachers,
});
