import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getMainHeader = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("mainHeader")
  );

export const getSubHeader = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("subHeader")
  );

export const getCurrentTable = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("currentTable")
  );

export const getCoursesBySchool = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("coursesBySchool").toJS()
  );

export const getStudentsBySchool = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("studentsBySchool").toJS()
  );

export const getTeachersBySchool = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("teachersBySchool").toJS()
  );

export const getCoursesByDepartment = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("coursesByDepartment").toJS()
  );

export const getSectionsByCourse = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("sectionsByCourse").toJS()
  );

export const getStudentsInSection = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("studentsInSection").toJS()
  );

export const getFilteredStudentsInSection = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("filteredStudentsInSection").toJS()
  );

export const getSectionsByTeacher = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("sectionsByTeacher").toJS()
  );

export const getSelectedSchoolRow = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("selectedSchoolRow").toJS()
  );

export const getSelectedCourseSection = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("selectedCourseSection").toJS()
  );

export const getNestedMainHeaders = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("nestedMainHeaders").toJS()
  );

export const getCoursesForStudent = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("coursesForStudent").toJS()
  );

export const getRoasterByTeacher = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("roasterByTeacher").toJS()
  );

export const getTeachersAssignments = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("teachersAssignments").toJS()
  );

export const getFilteredStudentBySchool = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("filteredStudentBySchool").toJS()
  );

export const getMasterSchedules = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("masterSchedules").toJS()
  );

export const getFilteredCoursesBySchool = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("filteredCoursesBySchool").toJS()
  );

export const getFilteredStudentsBySchool = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("filteredStudentsBySchool").toJS()
  );

export const getFilteredTeachersBySchool = () =>
  createSelector(selectors.studentClassState, (studentClassState) =>
    studentClassState.get("filteredTeachersBySchool").toJS()
  );
