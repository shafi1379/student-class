import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { Button } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import MaterialIcon from "../../../../generic/materialIcon";
import { CSVLink } from "react-csv";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import DataTable from "react-data-table-component";
import * as mainActions from "../../../../../app/actions";
import * as actions from "../../actions";
import * as selectors from "../../selectors";
import * as TableConfig from "../../../../../utilities/tableConfig";
import { API_URLS } from "../../../../../utilities/constants";
import * as Utilities from "../../../../../utilities/utilities";
import InfoModal from "../../../../generic/InfoModal";
import { anchorLinkStyle } from "../filters";

class Teachers extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "student-class.csv",
    },
    columns: TableConfig.teachersBySchool.map((dash, index) => {
      if (dash.selector === "sections") {
        dash.cell = (row) => (
          <span
            style={anchorLinkStyle}
            onClick={this.handleAction.bind(this, row, "sections")}
          >
            {row.sections}
          </span>
        );
      }
      if (dash.selector === "students") {
        dash.cell = (row) => (
          <span
            style={anchorLinkStyle}
            onClick={this.handleAction.bind(this, row, "students")}
          >
            {row.students}
          </span>
        );
      }
      return dash;
    }),
    openModel: false,
    selectedStudent: {},
    enteredVal: null,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.masterSchedules.length) {
      this.downloadCSV(nextProps.masterSchedules);
    }
  }

  handleAction = (row, cellName) => {
    if (cellName === "students") {
      this.props.loadRoasterByTeacher({
        school: row.schoolCode,
        teacherid: row["TeacherID"],
      });
      this.setState({
        openModel: true,
        selectedStudent: {
          ...row,
          studentID: row["TeacherID"],
          studentName: row["teacherName"],
        },
      });
    } else {
      this.props.setMainHeader(`Section for Teacher: ${row.teacherName}`);
      const mainHeaders = { ...this.props.nestedMainHeaders };
      this.props.setNestedMainHeaders({
        ...mainHeaders,
        "teachers-sections": `Section for Teacher: ${row.teacherName}`,
      });
      this.props.loadSectionsByTeacher({
        url: API_URLS.sectionsbyteacher,
        params: { school: row.schoolCode, teacherid: row.TeacherID },
      });
      this.props.setCurrentTable("teachers-sections");
    }
  };

  handleClose = () => {
    this.setState({
      openModel: false,
    });
  };

  loadDropdownValue = ({ enteredVal }) => {
    let newTeachersBySchool = [...this.props.teachersBySchool];
    if (enteredVal) {
      const attrs = [
        "teacherName",
        "TeacherID",
        "sections",
        "courses",
        "students",
      ];
      newTeachersBySchool = newTeachersBySchool.filter((f, index) => {
        return Utilities.returnBoolean(f, attrs, enteredVal);
      });
    }
    this.props.setFilteredTeachersBySchool(newTeachersBySchool);
    setTimeout(() => this.props.setProgressLoader(false), 300);
  };

  filterSearchField = (e) => {
    this.props.setProgressLoader(true);
    let value = e.target.value;
    this.setState({ enteredVal: value.trim() });
    this.loadDropdownValue({
      enteredVal: value.trim(),
    });
  };

  downloadCSV = (records) => {
    const currentTime = `_${new Date().getTime()}`;
    const totalRecords = records
      ? records
      : this.props.filteredTeachersBySchool;
    let columns = {};
    if (records) {
      columns = {
        "School Code": "schoolCode",
        "School Name": "schoolName",
        "Course#": "courseNumber",
        Title: "courseTitle",
        Section: "sectionNumber",
        Period: "periodName",
        Term: "termName",
        TeacherID: "TeacherID",
        "Teacher LastName": "teacherLastName",
        "Teacher FirstName": "teacherFirstName",
        Grade: "gradeLevel",
        "Start Date": "startDate",
        "End Date": "endDate",
        Inclusion: "InclusionCode",
        "Gifted Delivery": "GiftedDeliverModel",
        "ESOL Delivery": "ESOLDeliverModel",
        "EIP Delivery": "EIPDeliverModel",
        "Teacher 90 Percent": "Teacher90Percent",
        "# of Students": "students",
        "# of Student 90 Percent": "Student90Percent",
      };
    } else {
      columns = {
        "Teacher Name": "teacherName",
        "Teacher ID": "TeacherID",
        "# of Sections": "sections",
        "# of Courses": "courses",
        "# of Students": "students",
      };
    }
    const data = Utilities.generateExcelData(totalRecords || [], columns || {});
    data.unshift([this.props.selectedSchoolRow.schoolName]);
    data.unshift([
      records ? "Master Schedule Report" : "Teachers By School Summary",
    ]);
    this.setState({
      csvExport: { data, fileName: `Schedule_Analysis_${currentTime}` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
      this.props.setMasterSchedules([]);
    });
  };

  loadCSVFile = (param, data) => {
    if (param === "master") {
      this.props.setProgressLoader(true);
      this.props.loadMasterSchedules({
        school: this.props.selectedSchoolRow.schoolCode,
      });
    } else if (param === "roaster") {
      this.props.setProgressLoader(true);
      this.props.loadRoasterByTeacher({
        school: this.props.selectedSchoolRow.schoolCode,
        teacherid: "xxxx",
      });
    } else {
      this.props.loadTeachersAssignments({
        school: this.props.selectedSchoolRow.schoolCode,
      });
      this.setState({
        openModel: true,
        selectedStudent: {},
      });
    }
  };

  render() {
    const { columns, openModel, selectedStudent, csvExport } = this.state;
    const {
      roasterByTeacher,
      teachersAssignments,
      selectedSchoolRow,
      filteredTeachersBySchool,
    } = this.props;
    return (
      <Paper>
        <Grid
          container
          style={{
            paddingLeft: 15,
            display: "inline-flex",
            height: 50,
          }}
        >
          <Grid
            container
            item
            xs={2}
            style={{
              margin: "auto",
            }}
          >
            <Button
              className="button-style"
              variant="outlined"
              color="primary"
              size="small"
              onClick={this.loadCSVFile.bind(this, "master")}
            >
              Master Schedule
            </Button>
          </Grid>
          <Grid
            container
            item
            xs={2}
            style={{
              margin: "auto",
            }}
          >
            <Button
              className="button-style"
              variant="outlined"
              color="primary"
              size="small"
              onClick={this.loadCSVFile.bind(this, "roaster")}
            >
              Teacher Roasters
            </Button>
          </Grid>
          <Grid
            container
            item
            xs={2}
            style={{
              margin: "auto",
            }}
          >
            <Button
              className="button-style"
              variant="outlined"
              color="primary"
              size="small"
              onClick={this.loadCSVFile.bind(this, "assignments")}
            >
              Teacher Assignments
            </Button>
          </Grid>
          <Grid container item xs={4}>
            <Grid item xs={9} style={{ textAlign: "right", padding: 10 }}>
              <Typography variant="caption">
                <strong>Filter:</strong>
              </Typography>
            </Grid>
            <Grid item xs={3} style={{ marginTop: -10 }}>
              <TextField
                onChange={this.filterSearchField}
                style={{ width: 225 }}
                id="standard-basic"
                label="Search here..."
              />
            </Grid>
          </Grid>
          <Grid container item xs={2}>
            <span style={{ position: "absolute", right: 20 }}>
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  onClick={this.downloadCSV}
                  className="tools-btn"
                  color="primary"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
              <CSVLink
                ref={this.csvLink}
                data={csvExport.data}
                className="hidden"
                filename={csvExport.fileName}
                target="_self"
              />
            </span>
          </Grid>
        </Grid>
        <DataTable
          noHeader={true}
          striped={true}
          fixedHeader={true}
          className="dataTable-override"
          fixedHeaderScrollHeight={"60vh"}
          columns={columns}
          data={filteredTeachersBySchool || [{}]}
          customStyles={TableConfig.customStyles}
        />
        {roasterByTeacher.length > 0 && (
          <InfoModal
            open={openModel && roasterByTeacher.length > 0}
            columns={[...TableConfig.rosterByTeacherColumns]}
            tableData={roasterByTeacher || []}
            headerLabel={"Roster By Teacher "}
            student={selectedStudent}
            handleClose={(evt) => {
              this.props.setRoasterByTeacher([]);
              this.setState({ openModel: !openModel });
            }}
          />
        )}
        {this.props.teachersAssignments.length > 0 && (
          <InfoModal
            open={openModel && teachersAssignments.length > 0}
            columns={[...TableConfig.teachersAssignments]}
            tableData={teachersAssignments || []}
            headerLabel={
              "Teacher Assignments for : " + selectedSchoolRow.schoolName
            }
            student={selectedStudent}
            handleClose={(evt) => {
              this.props.setTeachersAssignments([]);
              this.setState({ openModel: !openModel });
            }}
          />
        )}
      </Paper>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    teachersBySchool: selectors.getTeachersBySchool(),
    filteredTeachersBySchool: selectors.getFilteredTeachersBySchool(),
    roasterByTeacher: selectors.getRoasterByTeacher(),
    teachersAssignments: selectors.getTeachersAssignments(),
    masterSchedules: selectors.getMasterSchedules(),
    nestedMainHeaders: selectors.getNestedMainHeaders(),
    selectedSchoolRow: selectors.getSelectedSchoolRow(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadRoasterByTeacher: (request) =>
    dispatch(actions.loadRoasterByTeacher(request)),
  loadTeachersAssignments: (request) =>
    dispatch(actions.loadTeachersAssignments(request)),
  loadMasterSchedules: (request) =>
    dispatch(actions.loadMasterSchedules(request)),
  setMasterSchedules: (data) => dispatch(actions.setMasterSchedules(data)),
  setRoasterByTeacher: (data) => dispatch(actions.setRoasterByTeacher(data)),
  setTeachersAssignments: (data) =>
    dispatch(actions.setTeachersAssignments(data)),
  setMainHeader: (request) => dispatch(actions.setMainHeader(request)),
  loadSectionsByTeacher: (request) =>
    dispatch(actions.loadSectionsByTeacher(request)),
  setCurrentTable: (request) => dispatch(actions.setCurrentTable(request)),
  setNestedMainHeaders: (request) =>
    dispatch(actions.setNestedMainHeaders(request)),
  setProgressLoader: (request) =>
    dispatch(mainActions.setProgressLoader(request)),
  setFilteredTeachersBySchool: (request) =>
    dispatch(actions.setFilteredTeachersBySchool(request)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Teachers);
