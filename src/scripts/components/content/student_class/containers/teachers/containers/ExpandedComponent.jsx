import React from "react";
import MaterialIcon from "../../../../../generic/materialIcon";

// eslint-disable-next-line react/prop-types
export default ({ data }) => (
  <>
    {data.children.map((obj, index) => (
      <div key={index} className="expanded-style">
        <div>
          <MaterialIcon icon="insert_drive_file_outlined" />
          <span>{obj["Section"]}</span>
        </div>
        <div>{obj.Course}</div>
        <div>{obj.Title}</div>
        <div>{obj.TermCode}</div>
        <div
          style={{
            color: "#4050B5",
            textDecoration: "underline",
            cursor: "pointer",
          }}
          onClick={(evt) => obj.handleAction(obj, "students")}
        >
          {obj.students}
        </div>
      </div>
    ))}
  </>
);
