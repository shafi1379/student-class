import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import DataTable from "react-data-table-component";
import * as actions from "../../../actions";
import * as selectors from "../../../selectors";
import * as TableConfig from "../../../../../../utilities/tableConfig";
import MaterialIcon from "../../../../../generic/materialIcon";
import { API_URLS } from "../../../../../../utilities/constants";
import ExpandedComponent from "./ExpandedComponent";
import { anchorLinkStyle } from "../../filters";

class SectionByTeacher extends Component {
  state = {
    columns: TableConfig.sectionsByTeacher.map((dash, index) => {
      if (dash.selector === "students") {
        dash.cell = (row) => (
          <span
            style={anchorLinkStyle}
            onClick={this.handleAction.bind(this, row, "sections")}
          >
            {row.students}
          </span>
        );
      }
      return dash;
    }),
  };

  handleAction = (row, cellName) => {
    const Course = row.Course;
    const Title = row.Title;
    this.props.setMainHeader(
      `Students In Section: ${Course} (${Title}), Section: ${row.Section}`
    );
    const mainHeaders = { ...this.props.nestedMainHeaders };
    this.props.setNestedMainHeaders({
      ...mainHeaders,
      "teachers-sections-students": `Students In Section: ${Course} (${Title}), Section: ${row.Section}`,
    });
    this.props.loadStudentsInSection({
      url: API_URLS.studentsinsection,
      params: {
        school: row.schoolCode,
        course: row.Course,
        section: row.Section,
      },
    });
    this.props.setCurrentTable("teachers-sections-students");
  };

  render() {
    const { columns } = this.state;
    const { sectionsByTeacher } = this.props;
    let expandedData = {};
    sectionsByTeacher.forEach((obj) => {
      if (Object.keys(expandedData).includes(obj["Period"] + "")) {
        expandedData[obj["Period"]]["children"].push({
          ...obj,
          handleAction: (row, cellName) => this.handleAction(row, cellName),
        });
        expandedData[obj["Period"]]["total"] =
          expandedData[obj["Period"]]["total"] + obj["students"];
      } else {
        expandedData[obj["Period"]] = {
          Section: obj["Period"] + "",
          total: obj["students"],
          children: [
            {
              ...obj,
              handleAction: (row, cellName) => this.handleAction(row, cellName),
            },
          ],
        };
      }
    });
    let expandedArray = Object.keys(expandedData)
      .map((key) => {
        return expandedData[key];
      })
      .map((obj) => {
        obj["Section"] = obj["Section"] + "(" + obj["total"] + ")";
        return obj;
      });
    return (
      <Paper>
        <DataTable
          noHeader={true}
          striped={true}
          fixedHeader={true}
          className="dataTable-override"
          fixedHeaderScrollHeight={"60vh"}
          columns={columns}
          data={expandedArray || [{}]}
          customStyles={TableConfig.customStyles}
          expandableRows={true}
          expandableRowExpanded={(row) => true}
          expandableIcon={{
            collapsed: (
              <MaterialIcon icon="folder" style={{ color: "#3f51b5" }} />
            ),
            expanded: (
              <MaterialIcon icon="folder_open" style={{ color: "#3f51b5" }} />
            ),
          }}
          expandableRowsComponent={
            <ExpandedComponent handleAction={this.handleAction} />
          }
        />
      </Paper>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    sectionsByTeacher: selectors.getSectionsByTeacher(),
    nestedMainHeaders: selectors.getNestedMainHeaders(),
  });
const mapDispatchToProps = (dispatch) => ({
  setMainHeader: (request) => dispatch(actions.setMainHeader(request)),
  loadStudentsInSection: (request) =>
    dispatch(actions.loadStudentsInSection(request)),
  setCurrentTable: (request) => dispatch(actions.setCurrentTable(request)),
  setNestedMainHeaders: (request) =>
    dispatch(actions.setNestedMainHeaders(request)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SectionByTeacher);
