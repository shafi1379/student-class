import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import MaterialIcon from "../../../../generic/materialIcon";
import { CSVLink } from "react-csv";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import DataTable from "react-data-table-component";
import Select from "react-select";
import * as mainActions from "../../../../../app/actions";
import * as actions from "../../actions";
import * as selectors from "../../selectors";
import * as TableConfig from "../../../../../utilities/tableConfig";
import * as Utilities from "../../../../../utilities/utilities";
import { selectStyles } from "../../../../generic/reactSelectCustomization";
import InfoModal from "../../../../generic/InfoModal";
import { anchorLinkStyle, classesFilter, gradeFilters } from "../filters";

class Students extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "student-class.csv",
    },
    columns: TableConfig.studentsBySchool.map((dash, index) => {
      if (dash.selector === "sections") {
        dash.cell = (row) => (
          <span
            style={anchorLinkStyle}
            onClick={this.handleAction.bind(this, row, "sections")}
          >
            {row.sections}
          </span>
        );
      }
      return dash;
    }),
    openModel: false,
    selectedStudent: {},
    gradeFiltersVal: gradeFilters[0].value,
    classesFilterVal: classesFilter[0].value,
    enteredVal: null,
  };

  handleAction = (row, cellName) => {
    this.props.loadCoursesForStudent({
      school: row.schoolCode,
      studentid: row["studentID"],
    });
    this.setState({
      openModel: true,
      selectedStudent: row,
    });
  };

  handleClose = () => {
    this.setState({
      openModel: false,
    });
  };

  handleDropdownChange = (param, { label, value }) => {
    this.props.setProgressLoader(true);
    if (param === "class") {
      this.setState({ classesFilterVal: value });
      this.loadDropdownValue({
        classesFilterVal: value,
        gradeFiltersVal: this.state.gradeFiltersVal,
        enteredVal: this.state.enteredVal,
      });
    } else {
      this.setState({ gradeFiltersVal: value });
      this.loadDropdownValue({
        classesFilterVal: this.state.classesFilterVal,
        gradeFiltersVal: value,
        enteredVal: this.state.enteredVal,
      });
    }
  };

  loadDropdownValue = ({ classesFilterVal, gradeFiltersVal, enteredVal }) => {
    let newStudentsBySchool = [...this.props.studentsBySchool];
    if (classesFilterVal) {
      if (classesFilterVal > 0) {
        newStudentsBySchool = newStudentsBySchool.filter(
          (f) => f.sections <= classesFilterVal
        );
      } else {
        if (classesFilterVal !== 0) {
          newStudentsBySchool = newStudentsBySchool.filter(
            (f) => f.missingCore === 1
          );
        }
      }
    }
    if (gradeFiltersVal) {
      if (gradeFiltersVal !== "all") {
        newStudentsBySchool = newStudentsBySchool.filter(
          (f) => f.Grade === gradeFiltersVal
        );
      }
    }
    if (enteredVal) {
      const attrs = [
        "studentID",
        "studentName",
        "Grade",
        "PROGRAM_CODES",
        "sections",
        "rd",
        "la",
        "ma",
        "sc",
        "ss",
      ];
      newStudentsBySchool = newStudentsBySchool.filter((f, index) => {
        return Utilities.returnBoolean(f, attrs, enteredVal);
      });
    }
    this.props.setFilteredStudentsBySchool(newStudentsBySchool);
    setTimeout(() => this.props.setProgressLoader(false), 300);
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const studentsBySchool = this.props.filteredStudentsBySchool;
    const columns = {
      "Student ID": "studentID",
      "Student Name": "studentName",
      "Grade Level": "Grade",
      "FTE Segments": "PROGRAM_CODES",
      "# of Classes": "sections",
      "Reading(k-5)": "rd",
      "Lang/Arts": "la",
      Maths: "ma",
      Science: "sc",
      "Soc Studies": "ss",
    };
    const data = Utilities.generateExcelData(
      studentsBySchool || [],
      columns || {}
    );
    data.unshift([this.props.selectedSchoolRow.schoolName]);
    data.unshift(["Students By School"]);
    this.setState({
      csvExport: { data, fileName: `Schedule_Analysis_${currentTime}` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  filterSearchField = (e) => {
    this.props.setProgressLoader(true);
    let value = e.target.value;
    this.setState({ enteredVal: value.trim() });
    this.loadDropdownValue({
      classesFilterVal: this.state.classesFilterVal,
      gradeFiltersVal: this.state.gradeFiltersVal,
      enteredVal: value.trim(),
    });
  };

  render() {
    const { columns, openModel, selectedStudent, csvExport } = this.state;
    const { currentTable, filteredStudentsBySchool } = this.props;
    return (
      <Paper>
        <Grid container style={{ display: "inline-flex", height: 50 }}>
          <Grid
            container
            item
            xs={3}
            style={{
              margin: "auto",
            }}
          >
            <Grid item xs={7} style={{ textAlign: "right", padding: 5 }}>
              <Typography variant="caption">
                <strong>Show Students with Courses:</strong>
              </Typography>
            </Grid>
            <Grid item xs={5}>
              <Select
                styles={selectStyles}
                // value={{}}
                isDisabled={currentTable === "missingCore"}
                defaultValue={classesFilter[0]}
                onChange={this.handleDropdownChange.bind(this, "class")}
                options={classesFilter}
              />
            </Grid>
          </Grid>
          <Grid
            container
            item
            xs={3}
            style={{
              margin: "auto",
            }}
          >
            <Grid item xs={6} style={{ textAlign: "right", padding: 5 }}>
              <Typography variant="caption">
                <strong>Filter By Grade:</strong>
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Select
                styles={selectStyles}
                // value={{}}
                defaultValue={gradeFilters[0]}
                onChange={this.handleDropdownChange.bind(this, "grade")}
                options={gradeFilters}
              />
            </Grid>
          </Grid>
          <Grid
            container
            item
            xs={4}
            style={{
              margin: "auto",
            }}
          >
            <Grid item xs={9} style={{ textAlign: "right", padding: 10 }}>
              <Typography variant="caption">
                <strong>Filter:</strong>
              </Typography>
            </Grid>
            <Grid item xs={3} style={{ marginTop: -10 }}>
              <TextField
                onChange={this.filterSearchField}
                style={{ width: 225 }}
                id="standard-basic"
                label="Search here..."
              />
            </Grid>
          </Grid>
          <Grid container item xs={2}>
            <span style={{ position: "absolute", right: 20 }}>
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  onClick={this.downloadCSV}
                  className="tools-btn"
                  color="primary"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
              <CSVLink
                ref={this.csvLink}
                data={csvExport.data}
                className="hidden"
                filename={csvExport.fileName}
                target="_self"
              />
            </span>
          </Grid>
        </Grid>
        <DataTable
          noHeader={true}
          striped={true}
          fixedHeader={true}
          className="dataTable-override"
          fixedHeaderScrollHeight={"60vh"}
          columns={columns}
          data={filteredStudentsBySchool || [{}]}
          customStyles={TableConfig.customStyles}
        />
        {this.props.coursesForStudent.length > 0 && (
          <InfoModal
            open={openModel && this.props.coursesForStudent.length > 0}
            columns={[...TableConfig.coursesForStudentColumns]}
            tableData={this.props.coursesForStudent || []}
            headerLabel={"Schedules for Student"}
            student={selectedStudent}
            handleClose={(evt) => {
              this.props.setCoursesForStudent([]);
              this.setState({ openModel: !openModel });
            }}
          />
        )}
      </Paper>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    studentsBySchool: selectors.getStudentsBySchool(),
    filteredStudentsBySchool: selectors.getFilteredStudentsBySchool(),
    coursesForStudent: selectors.getCoursesForStudent(),
    nestedMainHeaders: selectors.getNestedMainHeaders(),
    currentTable: selectors.getCurrentTable(),
    selectedSchoolRow: selectors.getSelectedSchoolRow(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadCoursesForStudent: (request) =>
    dispatch(actions.loadCoursesForStudent(request)),
  setCoursesForStudent: (data) => dispatch(actions.setCoursesForStudent(data)),
  setNestedMainHeaders: (request) =>
    dispatch(actions.setNestedMainHeaders(request)),
  setProgressLoader: (request) =>
    dispatch(mainActions.setProgressLoader(request)),
  setFilteredStudentsBySchool: (request) =>
    dispatch(actions.setFilteredStudentsBySchool(request)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Students);
