export const anchorLinkStyle = {
  color: "#683598",
  textDecoration: "underline",
  cursor: "pointer",
};

export const classesFilter = [
  {
    label: "No Filter",
    value: 0,
  },
  {
    label: "3 or fewer",
    value: 3,
  },
  {
    label: "4 or fewer",
    value: 4,
  },
  {
    label: "5 or fewer",
    value: 5,
  },
  {
    label: "6 or fewer",
    value: 6,
  },
  {
    label: "7 or fewer",
    value: 7,
  },
  {
    label: "Missing Core Courses",
    value: -1,
  },
];

export const gradeFilters = [
  {
    label: "All Grades",
    value: "all",
  },
  {
    label: "Pre-K",
    value: "PK",
  },
  {
    label: "Kindergarten",
    value: "KK",
  },
  {
    label: "01",
    value: "01",
  },
  {
    label: "02",
    value: "02",
  },
  {
    label: "03",
    value: "03",
  },
  {
    label: "04",
    value: "04",
  },
  {
    label: "05",
    value: "05",
  },
  {
    label: "06",
    value: "06",
  },
  {
    label: "07",
    value: "07",
  },
  {
    label: "08",
    value: "08",
  },
  {
    label: "09",
    value: "09",
  },
  {
    label: "10",
    value: "10",
  },
  {
    label: "11",
    value: "11",
  },
  {
    label: "12",
    value: "12",
  },
];

export const courseFilters = [
  {
    label: "All Courses",
    value: "all",
  },
  {
    label: "Remedial/EIP",
    value: "remedial",
  },
  {
    label: "Gifted",
    value: "gifted",
  },
  {
    label: "Distance Learning",
    value: "distlearn",
  },
  {
    label: "Vocational Labs",
    value: "vocational",
  },
  {
    label: ".8 and .9 (SPED) Courses",
    value: "sped",
  },
  {
    label: "Joint Enrollment Pvt Institution",
    value: "jointenroll",
  },
  {
    label: "Dual Enrollment",
    value: "dualenroll",
  },
];
