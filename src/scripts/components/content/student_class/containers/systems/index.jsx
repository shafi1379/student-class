import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import MaterialIcon from "../../../../generic/materialIcon";
import { CSVLink } from "react-csv";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import DataTable from "react-data-table-component";
import Select from "react-select";
import * as mainActions from "../../../../../app/actions";
import * as actions from "../../actions";
import * as selectors from "../../selectors";
import { API_URLS } from "../../../../../utilities/constants";
import * as TableConfig from "../../../../../utilities/tableConfig";
import * as Utilities from "../../../../../utilities/utilities";
import { selectStyles } from "../../../../generic/reactSelectCustomization";
import { anchorLinkStyle, courseFilters } from "../filters";

class Systems extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "student-class.csv",
    },
    columns: TableConfig.coursesBySystem.map((dash, index) => {
      if (dash.selector === "sections") {
        dash.cell = (row) => (
          <span
            style={anchorLinkStyle}
            onClick={this.handleAction.bind(this, row, "sections")}
          >
            {row.sections}
          </span>
        );
      }
      return dash;
    }),
    courseFilterVal: courseFilters[0].value,
    enteredVal: null,
  };

  handleAction = (row, cellName) => {
    this.props.setMainHeader(
      `Section for Course: ${row.Course} (${row.Title})`
    );
    const mainHeaders = { ...this.props.nestedMainHeaders };
    this.props.setNestedMainHeaders({
      ...mainHeaders,
      "systems-sections": `Section for Course: ${row.Course} (${row.Title})`,
    });
    this.props.setCurrentTable("systems-sections");
    this.props.loadSectionsByCourse({
      url: API_URLS.sectionsbycourse,
      params: {
        school: this.props.selectedSchoolRow.schoolCode,
        course: row.Course,
      },
    });
    this.props.setSelectedCourseSection(row);
  };

  handleDropdownChange = (param, { label, value }) => {
    this.props.setProgressLoader(true);
    this.setState({ courseFilterVal: value });
    this.loadDropdownValue({
      courseFilterVal: value,
      enteredVal: this.state.enteredVal,
    });
  };

  loadDropdownValue = ({ courseFilterVal, enteredVal }) => {
    let newCoursesBySchool = [...this.props.coursesBySchool];
    if (courseFilterVal) {
      if (courseFilterVal !== "all") {
        newCoursesBySchool = newCoursesBySchool.filter(
          (f) => f[courseFilterVal] > 0
        );
      }
    }
    if (enteredVal) {
      const attrs = [
        "Course",
        "Title",
        "Department",
        "schools",
        "sections",
        "maxClassSize",
        "minClassSize",
        "avgClassSize",
      ];
      newCoursesBySchool = newCoursesBySchool.filter((f, index) => {
        return Utilities.returnBoolean(f, attrs, enteredVal);
      });
    }
    this.props.setFilteredCoursesBySchool(newCoursesBySchool);
    setTimeout(() => this.props.setProgressLoader(false), 300);
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const coursesBySchool = this.props.filteredCoursesBySchool;
    const columns = {
      "Course #": "Course",
      Title: "Title",
      Department: "Department",
      "# of Schools": "schools",
      "# of Sections": "sections",
      Max: "maxClassSize",
      Min: "minClassSize",
      Avg: "avgClassSize",
    };
    const data = Utilities.generateExcelData(
      coursesBySchool || [],
      columns || {}
    );
    data.unshift([this.props.selectedSchoolRow.schoolName]);
    data.unshift(["Courses By Systems"]);
    this.setState({
      csvExport: { data, fileName: `Schedule_Analysis_${currentTime}` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  filterSearchField = (e) => {
    this.props.setProgressLoader(true);
    let value = e.target.value;
    this.setState({ enteredVal: value.trim() });
    this.loadDropdownValue({
      courseFilterVal: this.state.courseFilterVal,
      enteredVal: value.trim(),
    });
  };

  render() {
    const { columns, csvExport } = this.state;
    const { filteredCoursesBySchool } = this.props;
    return (
      <Paper>
        <Grid container style={{ marginTop: 10, height: 50 }}>
          <Grid
            container
            item
            xs={3}
            style={{
              margin: "auto",
            }}
          >
            <Grid item xs={5} style={{ textAlign: "right", padding: 5 }}>
              <Typography variant="caption">
                <strong>Filter By Course Type:</strong>
              </Typography>
            </Grid>
            <Grid item xs={7}>
              <Select
                styles={selectStyles}
                //value={{}}
                defaultValue={courseFilters[0]}
                onChange={this.handleDropdownChange.bind(this, "courseType")}
                options={[...courseFilters]}
              />
            </Grid>
          </Grid>
          <Grid
            container
            item
            xs={4}
            style={{
              margin: "auto",
            }}
          >
            <Grid item xs={9} style={{ textAlign: "right", padding: 10 }}>
              <Typography variant="caption">
                <strong>Filter:</strong>
              </Typography>
            </Grid>
            <Grid item xs={3} style={{ marginTop: -10 }}>
              <TextField
                onChange={this.filterSearchField}
                style={{ width: 225 }}
                id="standard-basic"
                label="Search here..."
              />
            </Grid>
          </Grid>
          <Grid container item xs={5}>
            <span style={{ position: "absolute", right: 20 }}>
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  onClick={this.downloadCSV}
                  className="tools-btn"
                  color="primary"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
              <CSVLink
                ref={this.csvLink}
                data={csvExport.data}
                className="hidden"
                filename={csvExport.fileName}
                target="_self"
              />
            </span>
          </Grid>
        </Grid>
        <DataTable
          noHeader={true}
          striped={true}
          fixedHeader={true}
          className="dataTable-override"
          fixedHeaderScrollHeight={"60vh"}
          columns={columns}
          data={filteredCoursesBySchool || [{}]}
          customStyles={TableConfig.customStyles}
        />
      </Paper>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    coursesBySchool: selectors.getCoursesBySchool(),
    selectedSchoolRow: selectors.getSelectedSchoolRow(),
    nestedMainHeaders: selectors.getNestedMainHeaders(),
    filteredCoursesBySchool: selectors.getFilteredCoursesBySchool(),
  });
const mapDispatchToProps = (dispatch) => ({
  setMainHeader: (request) => dispatch(actions.setMainHeader(request)),
  loadCoursesByDepartment: (request) =>
    dispatch(actions.loadCoursesByDepartment(request)),
  loadSectionsByCourse: (request) =>
    dispatch(actions.loadSectionsByCourse(request)),
  setSelectedCourseSection: (request) =>
    dispatch(actions.setSelectedCourseSection(request)),
  setCurrentTable: (request) => dispatch(actions.setCurrentTable(request)),
  setNestedMainHeaders: (request) =>
    dispatch(actions.setNestedMainHeaders(request)),
  setProgressLoader: (request) =>
    dispatch(mainActions.setProgressLoader(request)),
  setFilteredCoursesBySchool: (request) =>
    dispatch(actions.setFilteredCoursesBySchool(request)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Systems);
