import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import DataTable from "react-data-table-component";
import * as actions from "../../../actions";
import * as selectors from "../../../selectors";
import { API_URLS } from "../../../../../../utilities/constants";
import * as TableConfig from "../../../../../../utilities/tableConfig";

class CourseSections extends Component {
  state = {
    columns: TableConfig.sectionsByCourse.map((dash, index) => {
      if (dash.selector === "classSize") {
        dash.cell = (row) => (
          <span
            style={{
              color: "#4050B5",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleAction.bind(this, row, "department")}
          >
            {row.classSize}
          </span>
        );
      }
      return dash;
    }),
  };

  handleAction = (row, cellName) => {
    const { Course, Title } = this.props.selectedCourseSection;
    this.props.setMainHeader(
      `Students In Section: ${Course} (${Title}), Section: ${row.Section}`
    );
    const mainHeaders = { ...this.props.nestedMainHeaders };
    this.props.setNestedMainHeaders({
      ...mainHeaders,
      "sections-sections-students": `Students In Section: ${Course} (${Title}), Section: ${row.Section}`,
    });
    this.props.loadStudentsInSection({
      url: API_URLS.studentsinsection,
      params: { school: row.schoolCode, course: Course, section: row.Section },
    });
    this.props.setCurrentTable("sections-sections-students");
  };

  render() {
    const { columns } = this.state;
    const { sectionsByCourse } = this.props;
    return (
      <DataTable
        noHeader={true}
        striped={true}
        fixedHeader={true}
        className="dataTable-override"
        fixedHeaderScrollHeight={"68vh"}
        columns={columns}
        data={sectionsByCourse || [{}]}
        customStyles={TableConfig.customStyles}
      />
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    sectionsByCourse: selectors.getSectionsByCourse(),
    selectedCourseSection: selectors.getSelectedCourseSection(),
    currentTable: selectors.getCurrentTable(),
    nestedMainHeaders: selectors.getNestedMainHeaders(),
  });
const mapDispatchToProps = (dispatch) => ({
  setMainHeader: (request) => dispatch(actions.setMainHeader(request)),
  loadStudentsInSection: (request) =>
    dispatch(actions.loadStudentsInSection(request)),
  setCurrentTable: (request) => dispatch(actions.setCurrentTable(request)),
  setNestedMainHeaders: (request) =>
    dispatch(actions.setNestedMainHeaders(request)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CourseSections);
