import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import MaterialIcon from "../../../../../generic/materialIcon";
import { CSVLink } from "react-csv";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import DataTable from "react-data-table-component";
import * as mainActions from "../../../../../../app/actions";
import * as actions from "../../../actions";
import * as selectors from "../../../selectors";
import * as TableConfig from "../../../../../../utilities/tableConfig";
import * as Utilities from "../../../../../../utilities/utilities";
import InfoModal from "../../../../../generic/InfoModal";

class StudentInSections extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "student-class.csv",
    },
    columns: TableConfig.studentsInSection.map((dash, index) => {
      if (dash.selector === "studentID") {
        dash.cell = (row) => (
          <span
            style={{
              color: "#4050B5",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleAction.bind(this, row, "studentID")}
          >
            ******{row.studentID.substr(row.studentID.length - 4)}
          </span>
        );
      }
      return dash;
    }),
    openModel: false,
    selectedStudent: {},
    enteredVal: null,
  };

  handleAction = (row, cellName) => {
    this.props.loadCoursesForStudent({
      school: row.schoolCode,
      studentid: row[cellName],
    });
    this.setState({
      openModel: true,
      selectedStudent: row,
    });
  };

  handleClose = () => {
    this.setState({
      openModel: false,
    });
  };

  filterSearchField = (e) => {
    this.props.setProgressLoader(true);
    let value = e.target.value;
    this.setState({ enteredVal: value.trim() });
    this.loadDropdownValue({
      enteredVal: value.trim(),
    });
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const studentsInSection = this.props.filteredStudentsInSection;
    const columns = {
      "Class Period": "Period",
      "Teacher Name": "teacherName",
      "Student Id": "studentID",
      "Student Name": "studentName",
      Grade: "Grade",
      "FTE Segment": "PROGRAM_CODES",
    };
    const data = Utilities.generateExcelData(
      studentsInSection || [],
      columns || {}
    );
    data.unshift([this.props.selectedSchoolRow.schoolName]);
    const mainHeaders = { ...this.props.nestedMainHeaders };
    data.unshift([mainHeaders["sections-sections-students"]]);
    this.setState({
      csvExport: { data, fileName: `Schedule_Analysis_${currentTime}` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  loadDropdownValue = ({ enteredVal }) => {
    let newStudentsInSection = [...this.props.studentsInSection];
    if (enteredVal) {
      const attrs = [
        "Period",
        "teacherName",
        "studentID",
        "studentName",
        "Grade",
        "PROGRAM_CODES",
      ];
      newStudentsInSection = newStudentsInSection.filter((f, index) => {
        return Utilities.returnBoolean(f, attrs, enteredVal);
      });
    }
    this.props.setFilteredStudentsInSection(newStudentsInSection);
    setTimeout(() => this.props.setProgressLoader(false), 300);
  };

  render() {
    const { columns, openModel, selectedStudent, csvExport } = this.state;
    const { filteredStudentsInSection } = this.props;
    return (
      <Paper>
        <Grid container style={{ marginTop: 10, height: 50 }}>
          <Grid container item xs={10}>
            <Grid item xs={9} style={{ textAlign: "right", padding: 10 }}>
              <Typography variant="caption">
                <strong>Filter:</strong>
              </Typography>
            </Grid>
            <Grid item xs={3} style={{ marginTop: -10 }}>
              <TextField
                onChange={this.filterSearchField}
                style={{ width: 225 }}
                id="standard-basic"
                label="Search here..."
              />
            </Grid>
          </Grid>
          <Grid container item xs={2}>
            <span style={{ position: "absolute", right: 20 }}>
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  onClick={this.downloadCSV}
                  className="tools-btn"
                  color="primary"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
              <CSVLink
                ref={this.csvLink}
                data={csvExport.data}
                className="hidden"
                filename={csvExport.fileName}
                target="_self"
              />
            </span>
          </Grid>
        </Grid>
        <DataTable
          noHeader={true}
          striped={true}
          fixedHeader={true}
          className="dataTable-override"
          fixedHeaderScrollHeight={"60vh"}
          columns={columns}
          data={filteredStudentsInSection || [{}]}
          customStyles={TableConfig.customStyles}
        />
        {this.props.coursesForStudent.length > 0 && (
          <InfoModal
            open={openModel && this.props.coursesForStudent.length > 0}
            columns={[...TableConfig.coursesForStudentColumns]}
            tableData={this.props.coursesForStudent || []}
            headerLabel={"Schedules for Student"}
            student={selectedStudent}
            handleClose={(evt) => {
              this.props.setCoursesForStudent([]);
              this.setState({ openModel: !openModel });
            }}
          />
        )}
      </Paper>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    studentsInSection: selectors.getStudentsInSection(),
    filteredStudentsInSection: selectors.getFilteredStudentsInSection(),
    coursesForStudent: selectors.getCoursesForStudent(),
    selectedSchoolRow: selectors.getSelectedSchoolRow(),
    nestedMainHeaders: selectors.getNestedMainHeaders(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadCoursesForStudent: (request) =>
    dispatch(actions.loadCoursesForStudent(request)),
  setCoursesForStudent: (data) => dispatch(actions.setCoursesForStudent(data)),
  setFilteredStudentsInSection: (request) =>
    dispatch(actions.setFilteredStudentsInSection(request)),
  setProgressLoader: (request) =>
    dispatch(mainActions.setProgressLoader(request)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StudentInSections);
