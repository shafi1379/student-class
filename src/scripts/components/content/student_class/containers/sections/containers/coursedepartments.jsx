import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import DataTable from "react-data-table-component";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import MaterialIcon from "../../../../../generic/materialIcon";
import { CSVLink } from "react-csv";
import * as selectors from "../../../selectors";
import * as TableConfig from "../../../../../../utilities/tableConfig";
import * as Utilities from "../../../../../../utilities/utilities";

class CourseDepartments extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "student-class.csv",
    },
    columns: TableConfig.coursesByDepartment.map((dash, index) => {
      if (dash.selector === "Department") {
        dash.cell = (row) => (
          <span
            style={{
              color: "#4050B5",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleAction.bind(this, row, "department")}
          >
            {row.Department}
          </span>
        );
      }
      return dash;
    }),
  };

  handleAction = (row, cellName) => {};

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const coursesByDepartment = this.props.coursesByDepartment;
    const columns = {
      "Course #": "Course",
      "Course Title": "Title",
      "Teacher ID": "TeacherID",
      "Teacher Name": "teacherName",
      "# of Sections": "sections",
      "# of Students": "students",
    };
    const data = Utilities.generateExcelData(
      coursesByDepartment || [],
      columns || {}
    );
    data.unshift([this.props.selectedSchoolRow.schoolName]);
    const mainHeaders = { ...this.props.nestedMainHeaders };
    data.unshift([mainHeaders["sections-departments"]]);
    this.setState({
      csvExport: { data, fileName: `Schedule_Analysis_${currentTime}` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  render() {
    const { columns, csvExport } = this.state;
    const { coursesByDepartment } = this.props;
    return (
      <Paper>
        <Grid container style={{ marginTop: 10, height: 50 }}>
          <Grid container item xs={12}>
            <span style={{ position: "absolute", right: 20 }}>
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  onClick={this.downloadCSV}
                  className="tools-btn"
                  color="primary"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
              <CSVLink
                ref={this.csvLink}
                data={csvExport.data}
                className="hidden"
                filename={csvExport.fileName}
                target="_self"
              />
            </span>
          </Grid>
        </Grid>
        <DataTable
          noHeader={true}
          striped={true}
          fixedHeader={true}
          className="dataTable-override"
          fixedHeaderScrollHeight={"60vh"}
          columns={columns}
          data={coursesByDepartment || [{}]}
          customStyles={TableConfig.customStyles}
        />
      </Paper>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    coursesByDepartment: selectors.getCoursesByDepartment(),
    selectedSchoolRow: selectors.getSelectedSchoolRow(),
    nestedMainHeaders: selectors.getNestedMainHeaders(),
  });
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CourseDepartments);
