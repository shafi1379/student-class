import { fromJS } from "immutable";
import {
  SET_MAIN_HEADER,
  SET_SUB_HEADER,
  SET_COURSES_BY_SCHOOL,
  SET_STUDENTS_BY_SCHOOL,
  SET_FILTERED_STUDENT_BY_SCHOOL,
  SET_TEACHERS_BY_SCHOOL,
  SET_FILTERED_COURSES_BY_SCHOOL,
  SET_FILTERED_STUDENTS_BY_SCHOOL,
  SET_FILTERED_TEACHERS_BY_SCHOOL,
  SET_CURRENT_TABLE,
  SET_COURSES_BY_DEPARTMENT,
  SET_SECTIONS_BY_COURSE,
  SET_STUDENTS_IN_SECTION,
  SET_FILTERED_STUDENTS_IN_SECTION,
  SET_SELECTED_SCHOOL_ROW,
  SET_SELECTED_COURSE_SECTION,
  SET_SECTIONS_BY_TEACHER,
  SET_NESTED_MAIN_HEADERS,
  SET_COURSES_FOR_STUDENT,
  SET_ROASTER_BY_TEACHER,
  SET_TEACHERS_ASSIGNMENTS,
  SET_MASTER_SCHEDULES,
} from "./actions";

const initialState = fromJS({
  mainHeader: "Section By School Summary",
  subHeader: "System Totals",
  currentTable: null,
  selectedSchoolRow: fromJS({}),
  selectedCourseSection: fromJS({}),
  coursesBySchool: [],
  studentsBySchool: [],
  teachersBySchool: [],
  filteredCoursesBySchool: [],
  filteredStudentsBySchool: [],
  filteredTeachersBySchool: [],
  filteredStudentBySchool: [],
  coursesByDepartment: [],
  sectionsByCourse: [],
  studentsInSection: [],
  filteredStudentsInSection: [],
  sectionsByTeacher: [],
  coursesForStudent: [],
  roasterByTeacher: [],
  teachersAssignments: [],
  masterSchedules: [],
  nestedMainHeaders: fromJS({ default: "Section By School Summary" }),
});

const attendance = (state = initialState, action) => {
  switch (action.type) {
    case SET_MAIN_HEADER:
      return state.set("mainHeader", action.mainHeader);
    case SET_SUB_HEADER:
      return state.set("subHeader", action.subHeader);
    case SET_CURRENT_TABLE:
      return state.set("currentTable", action.table);
    case SET_COURSES_BY_SCHOOL:
      return state.set("coursesBySchool", fromJS([...action.courses]));
    case SET_COURSES_FOR_STUDENT:
      return state.set("coursesForStudent", fromJS([...action.courses]));
    case SET_STUDENTS_BY_SCHOOL:
      return state.set("studentsBySchool", fromJS([...action.students]));
    case SET_FILTERED_COURSES_BY_SCHOOL:
      return state.set("filteredCoursesBySchool", fromJS([...action.courses]));
    case SET_FILTERED_STUDENTS_BY_SCHOOL:
      return state.set(
        "filteredStudentsBySchool",
        fromJS([...action.students])
      );
    case SET_FILTERED_TEACHERS_BY_SCHOOL:
      return state.set(
        "filteredTeachersBySchool",
        fromJS([...action.teachers])
      );
    case SET_FILTERED_STUDENT_BY_SCHOOL:
      return state.set("filteredStudentBySchool", fromJS([...action.students]));
    case SET_TEACHERS_BY_SCHOOL:
      return state.set("teachersBySchool", fromJS([...action.teachers]));
    case SET_COURSES_BY_DEPARTMENT:
      return state.set("coursesByDepartment", fromJS([...action.departments]));
    case SET_SECTIONS_BY_COURSE:
      return state.set("sectionsByCourse", fromJS([...action.sections]));
    case SET_STUDENTS_IN_SECTION:
      return state.set("studentsInSection", fromJS([...action.students]));
    case SET_FILTERED_STUDENTS_IN_SECTION:
      return state.set(
        "filteredStudentsInSection",
        fromJS([...action.students])
      );
    case SET_SECTIONS_BY_TEACHER:
      return state.set("sectionsByTeacher", fromJS([...action.teachers]));
    case SET_ROASTER_BY_TEACHER:
      return state.set(
        "roasterByTeacher",
        fromJS([...action.roasterByTeacher])
      );
    case SET_TEACHERS_ASSIGNMENTS:
      return state.set(
        "teachersAssignments",
        fromJS([...action.teachersAssignments])
      );
    case SET_SELECTED_SCHOOL_ROW:
      return state.set("selectedSchoolRow", fromJS({ ...action.schoolRow }));
    case SET_SELECTED_COURSE_SECTION:
      return state.set(
        "selectedCourseSection",
        fromJS({ ...action.courseDetails })
      );
    case SET_NESTED_MAIN_HEADERS:
      return state.set("nestedMainHeaders", fromJS({ ...action.headers }));
    case SET_MASTER_SCHEDULES:
      return state.set("masterSchedules", fromJS([...action.masterSchedules]));
    default:
      return state;
  }
};

export default attendance;
