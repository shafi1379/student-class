import { fromJS } from "immutable";
import {
  SET_ERROR_SUMMARY_ERR,
  SET_ERROR_DETAILS,
  SET_CURRENT_TABLE,
  SET_SELECTED_ERR_CODE,
} from "./actions";

const initialState = fromJS({
  schoolList: [],
  errorSummaryErr: [],
  errorDetails: [],
  currentTable: null,
  selectedErrCode: {},
});

const attendance = (state = initialState, action) => {
  switch (action.type) {
    case SET_ERROR_SUMMARY_ERR:
      return state.set("errorSummaryErr", fromJS([...action.errorList]));
    case SET_ERROR_DETAILS:
      return state.set("errorDetails", fromJS([...action.errDetails]));
    case SET_SELECTED_ERR_CODE:
      return state.set("selectedErrCode", fromJS({ ...action.errCode }));
    case SET_CURRENT_TABLE:
      return state.set("currentTable", action.table);
    default:
      return state;
  }
};

export default attendance;
