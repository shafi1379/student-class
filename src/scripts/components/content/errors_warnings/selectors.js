import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getErrorSummaryErr = () =>
  createSelector(selectors.errWarnState, (errWarnState) =>
    errWarnState.get("errorSummaryErr").toJS()
  );

export const getErrorDetails = () =>
  createSelector(selectors.errWarnState, (errWarnState) =>
    errWarnState.get("errorDetails").toJS()
  );

export const getCurrentTable = () =>
  createSelector(selectors.errWarnState, (errWarnState) =>
    errWarnState.get("currentTable")
  );

export const getSelectedErrCode = () =>
  createSelector(selectors.errWarnState, (errWarnState) =>
    errWarnState.get("selectedErrCode").toJS()
  );
