import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import DataTable from "react-data-table-component";
import * as appSelectors from "../../../app/selectors";
import * as actions from "./actions";
import * as selectors from "./selectors";
import * as TableConfig from "../../../utilities/tableConfig";
import { API_URLS } from "../../../utilities/constants";
import ErrWarnSchool from "./containers/errwarnschool";
import ErrWarnStudent from "./containers/errwarnstudent";

class ErrorsWarnings extends Component {
  state = {
    columns: TableConfig.errorsColumns.map((dash, index) => {
      if (dash.selector === "errCount") {
        dash.cell = (row) => (
          <span
            style={{
              color: "#4050B5",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleAction.bind(this, row, "errCount")}
          >
            {row.errCount}
          </span>
        );
      }
      return dash;
    }),
  };

  handleAction = (row, cellName) => {
    this.props.loadErrorSummaryErr({
      url: API_URLS.errorsummaryerr,
      params: { school: this.props.loginData.school, errCode: row.errCode },
    });
    this.props.setSelectedErrCode(row);
    this.props.setCurrentTable("errWarn-school");
  };

  render() {
    const { columns } = this.state;
    const { errorsList, currentTable } = this.props;
    let dataTable = null;
    if (currentTable === "errWarn-school") {
      dataTable = <ErrWarnSchool />;
    } else if (currentTable === "errWarn-school-student") {
      dataTable = <ErrWarnStudent />;
    } else {
      dataTable = (
        <DataTable
          noHeader={true}
          striped={true}
          fixedHeader={true}
          className="dataTable-override"
          fixedHeaderScrollHeight={"75vh"}
          columns={columns}
          data={errorsList || [{}]}
          customStyles={TableConfig.customStyles}
        />
      );
    }
    return <Paper className="errors-warnings mr-tp15">{dataTable}</Paper>;
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    loginData: appSelectors.getLoginData(),
    errorsList: appSelectors.getErrorsList(),
    currentTable: selectors.getCurrentTable(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadErrorSummaryErr: (request) =>
    dispatch(actions.loadErrorSummaryErr(request)),
  setCurrentTable: (request) => dispatch(actions.setCurrentTable(request)),
  setSelectedErrCode: (request) =>
    dispatch(actions.setSelectedErrCode(request)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ErrorsWarnings));
