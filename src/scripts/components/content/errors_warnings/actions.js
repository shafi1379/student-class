export const LOAD_ERROR_SUMMARY_ERR = "LOAD_ERROR_SUMMARY_ERR";
export const loadErrorSummaryErr = (request) => ({
  type: LOAD_ERROR_SUMMARY_ERR,
  request,
});

export const SET_ERROR_SUMMARY_ERR = "SET_ERROR_SUMMARY_ERR";
export const setErrorSummaryErr = (errorList) => ({
  type: SET_ERROR_SUMMARY_ERR,
  errorList,
});

export const LOAD_ERROR_DETAILS = "LOAD_ERROR_DETAILS";
export const loadErrorDetails = (request) => ({
  type: LOAD_ERROR_DETAILS,
  request,
});

export const SET_ERROR_DETAILS = "SET_ERROR_DETAILS";
export const setErrorDetails = (errDetails) => ({
  type: SET_ERROR_DETAILS,
  errDetails,
});

export const SET_CURRENT_TABLE = "SET_CURRENT_TABLE";
export const setCurrentTable = (table) => ({
  type: SET_CURRENT_TABLE,
  table,
});

export const SET_SELECTED_ERR_CODE = "SET_SELECTED_ERR_CODE";
export const setSelectedErrCode = (errCode) => ({
  type: SET_SELECTED_ERR_CODE,
  errCode,
});
