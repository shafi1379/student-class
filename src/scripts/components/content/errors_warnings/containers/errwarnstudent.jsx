import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import DataTable from "react-data-table-component";
import * as actions from "./../actions";
import * as selectors from "./../selectors";
import MaterialIcon from "../../../generic/materialIcon";
import * as TableConfig from "../../../../utilities/tableConfig";
import * as Utilities from "../../../../utilities/utilities";
import { CSVLink } from "react-csv";

class ErrWarnStudent extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "Schedule_Analysis.csv",
    },
    columns: TableConfig.errWarnStudent.map((dash, index) => {
      if (dash.selector === "errCount") {
        dash.cell = (row) => (
          <span
            style={{
              color: "#4050B5",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleAction.bind(this, row, "errCount")}
          >
            {row.errCount}
          </span>
        );
      }
      return dash;
    }),
  };

  handleAction = (row, cellName) => {};

  goBack = () => {
    const currentTable = this.props.currentTable;
    const lastIndex = currentTable.lastIndexOf("-");
    const route = currentTable.substr(0, lastIndex);
    this.props.setCurrentTable(route);
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const errorsDetails = this.props.errorsDetails;
    const columns = {
      "Student ID": "StudentID",
      "Student Name": "studentName",
      "Course #": "courseNumber",
      "Field Name": "FIELD_NAME",
      "Field Content": "FIELD_CONTENT",
    };
    const data = Utilities.generateExcelData(
      errorsDetails || [],
      columns || {}
    );
    data.unshift([
      `School: ${
        this.props.errorsDetails && this.props.errorsDetails[0].schoolName
      }`,
    ]);
    data.unshift([this.props.selectedErrCode.errorDesc]);
    this.setState({
      csvExport: { data, fileName: `Schedule_Analysis_${currentTime}` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  render() {
    const { columns, csvExport } = this.state;
    const { errorsDetails, currentTable, selectedErrCode } = this.props;
    return (
      <React.Fragment>
        <Grid container>
          <Grid container item xs={2}>
            {currentTable && (
              <strong
                style={{
                  color: "#4050B5",
                  textDecoration: "underline",
                  cursor: "pointer",
                  padding: 15,
                }}
                onClick={this.goBack}
              >
                Back
              </strong>
            )}
          </Grid>
          <Grid container item xs={8}>
            <Typography
              variant="caption"
              style={{ padding: 15, margin: "auto" }}
            >
              <strong>
                {selectedErrCode.errorDesc}
                <Typography
                  variant="caption"
                  style={{ textAlign: "center", display: "block" }}
                >
                  <strong>Resolution: {selectedErrCode.fixDescription}</strong>
                </Typography>
                <Typography
                  variant="caption"
                  style={{ textAlign: "center", display: "block" }}
                >
                  <strong>
                    School:{" "}
                    {errorsDetails.length && errorsDetails[0].schoolName}
                  </strong>
                </Typography>
              </strong>
            </Typography>
          </Grid>
          <Grid container item xs={2}>
            <Grid item xs={12} style={{ textAlign: "right", paddingRight: 15 }}>
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  onClick={this.downloadCSV}
                  className="tools-btn"
                  color="primary"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
              <CSVLink
                ref={this.csvLink}
                data={csvExport.data}
                className="hidden"
                filename={csvExport.fileName}
                target="_self"
              />
            </Grid>
          </Grid>
        </Grid>
        <Paper className="errors-warnings">
          <DataTable
            noHeader={true}
            striped={true}
            fixedHeader={true}
            className="dataTable-override"
            fixedHeaderScrollHeight={"62vh"}
            columns={columns}
            data={errorsDetails || [{}]}
            customStyles={TableConfig.customStyles}
          />
        </Paper>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    errorsDetails: selectors.getErrorDetails(),
    selectedErrCode: selectors.getSelectedErrCode(),
    currentTable: selectors.getCurrentTable(),
  });
const mapDispatchToProps = (dispatch) => ({
  setCurrentTable: (request) => dispatch(actions.setCurrentTable(request)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ErrWarnStudent));
