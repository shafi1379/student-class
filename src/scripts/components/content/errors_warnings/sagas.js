import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  LOAD_ERROR_SUMMARY_ERR,
  LOAD_ERROR_DETAILS,
  setErrorSummaryErr,
  setErrorDetails,
} from "./actions";
import { setProgressLoader } from "../../../app/actions";
import * as API from "../../../services/api";

function* loadErrorDetailsRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setErrorDetails(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* errorDetailsListener() {
  yield takeLatest(LOAD_ERROR_DETAILS, loadErrorDetailsRequested);
}

function* loadErrorSummaryErrRequested({ request }) {
  try {
    yield put(setProgressLoader(true));
    const response = yield call(API.doPostRequest, request.url, request.params);
    yield put(setErrorSummaryErr(response));
    yield put(setProgressLoader(false));
  } catch (error) {
    console.error(error);
  }
}

function* errorSummaryErrListListener() {
  yield takeLatest(LOAD_ERROR_SUMMARY_ERR, loadErrorSummaryErrRequested);
}

export default function* root() {
  yield all([fork(errorSummaryErrListListener), fork(errorDetailsListener)]);
}
