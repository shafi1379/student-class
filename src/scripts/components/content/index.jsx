import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { withRouter } from "react-router-dom";
import { createStyles, withStyles } from "@material-ui/core/styles";
import { BrowserRouter as Router, Switch, Link } from "react-router-dom";
import RouteWithSubRoutes from "../generic/subRoutes";
import LoadingIndicator from "../generic/loadingIndicator";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as appSelectors from "../../app/selectors";
import { routes } from "./mainRoutes";

const styles = (theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      maxWidth: 350,
      margin: "0 auto",
    },
  });

class RouteConfig extends Component {
  state = { value: "student-class" };

  componentWillMount() {
    this.props.history.replace("/student-class/student-class");
  }

  handleCallToRouter = (event, newValue) => {
    this.setState({ value: newValue });
  };

  render() {
    const { value } = this.state;
    const { progressLoader, classes } = this.props;
    return (
      <div className="app-content">
        {progressLoader && <LoadingIndicator />}
        <Router basename="/student-class">
          <Paper className={classes.root} square>
            <Tabs
              value={value}
              onChange={this.handleCallToRouter.bind(value)}
              variant="fullWidth"
              indicatorColor="primary"
              textColor="primary"
              aria-label="icon tabs"
            >
              <Tab
                label="Student Class"
                className="minWidth90"
                value="student-class"
                to="/student-class"
                component={Link}
              />
              <Tab
                label="Errors and Warnings"
                className="minWidth90"
                value="errors-warnings"
                to="/errors-warnings"
                component={Link}
              />
            </Tabs>
          </Paper>
          <Switch>
            {routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route} />
            ))}
          </Switch>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    progressLoader: appSelectors.getProgressLoader(),
  });

const RouteConfigStyle = withStyles(styles)(RouteConfig);
export default connect(mapStateToProps, null)(withRouter(RouteConfigStyle));
