import StudentClass from "./student_class";
import ErrorsWarnings from "./errors_warnings";

export const routes = [
  {
    path: "/student-class",
    component: StudentClass,
  },
  {
    path: "/errors-warnings",
    component: ErrorsWarnings,
  },
];
