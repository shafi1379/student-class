import React, { FunctionComponent } from "react";
import Typography from "@material-ui/core/Typography";
import withWidth, { isWidthUp } from "@material-ui/core/withWidth";

const AppFooter: FunctionComponent = (props: any) => {
  let fontSize = isWidthUp("sm", props.width) ? "" : ".6rem";
  return (
    <div className="footer-content">
      <Typography
        variant="caption"
        style={{ fontSize: fontSize, color: "#ffffff" }}
      >
        ©&nbsp;{new Date().getFullYear()}&nbsp;
        <a
          style={{ color: "#ffffff" }}
          rel="noopener noreferrer"
          href="http://www.ushasoftware.com/index.php"
          target="_blank"
        >
          USHA Software
        </a>{" "}
        - All Rights Reserved
      </Typography>
    </div>
  );
};

export default withWidth()(AppFooter);
