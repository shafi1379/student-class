export const selectStyles = {
  input: (styles) => ({
    ...styles,
    padding: 0,
  }),
  option: (provided, state) => ({
    ...provided,
    padding: "3px 8px",
    fontSize: 12,
  }),
  control: (styles) => ({
    ...styles,
    minHeight: 20,
    padding: 0,
  }),
  valueContainer: (styles) => ({
    ...styles,
    fontSize: 12,
  }),
  indicatorContainer: (styles) => ({
    ...styles,
    padding: 4,
  }),
};
