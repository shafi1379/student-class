import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import DataTable from "react-data-table-component";
const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function InfoModalDialog(props) {
  const classes = useStyles();
  const handleClose = () => {
    props.handleClose();
  };

  const getHeaderName = () => {
    let headerName = props.headerLabel;
    if (props.student.studentID) {
      headerName +=
        " : *****" +
        props.student.studentID.substr(props.student.studentID.length - 4) +
        `(${props.student.studentName})`;
    }
    return headerName;
  };

  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        maxWidth={"lg"}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              {getHeaderName()}
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent
          dividers
          style={{ width: 930, height: "auto", maxHeight: 550, padding: 0 }}
        >
          <DataTable
            noHeader={true}
            striped={true}
            fixedHeader={true}
            fixedHeaderScrollHeight={"70vh"}
            columns={props.columns || []}
            className="dataTable-override dataTable-grey-header"
            data={props.tableData || []}
          />
        </DialogContent>
      </Dialog>
    </>
  );
}
