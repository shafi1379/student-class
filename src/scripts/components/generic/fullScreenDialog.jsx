import React, { Component } from "react";
import createStyles from "@material-ui/styles/createStyles";
import WithStyles from "@material-ui/styles/WithStyles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import { CSVLink } from "react-csv";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "../../app/selectors";
import * as actions from "../../app/actions";
import * as loginSelectors from "../../app/selectors";
import StudentDetailsDialog from "./details_dialog";

const useStyles = (theme) =>
  createStyles({
    appBar: {
      position: "fixed",
    },
  });

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class FullScreenDialog extends Component {
  csvLink = React.createRef();

  render() {
    return (
      <Dialog
        className="full-screen-dialog"
        fullScreen
        //open={open}
        //onClose={this.handleClose}
        //TransitionComponent={Transition}
      ></Dialog>
    );
  }
}

const mapStateToProps = () => createStructuredSelector({});
const mapDispatchToProps = (dispatch) => ({});
const styleFullScreenDialog = WithStyles(useStyles)(FullScreenDialog);
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(styleFullScreenDialog);
